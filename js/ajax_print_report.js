function printClick()
{
	var tgl_awal = $('#txt_tgl_awal').val();
	var tgl_akhir = $('#txt_tgl_akhir').val();
  var namatabel = $('#txt_select_report').val();
  var thn = $('#txt_thn_laporan').val();

  if(namatabel != "" && tgl_awal != "" && tgl_akhir != "" && thn != "")
  {
            if (namatabel == 'ii')
            {
              var link = "../../back-end/print_report/print_report_ii_code.php";
              var jns = "INCOMING";
              var jns2 = "INTERNAL";
            }
            else if (namatabel == 'ie')
            {
              var link = "../../back-end/print_report/print_report_ie_code.php";
              var jns = "INCOMING";
              var jns2 = "EXTERNAL";
            }
            else if (namatabel == 'oi')
            {
              var link = "../../back-end/print_report/print_report_oi_code.php";
              var jns = "OUTGOING";
              var jns2 = "INTERNAL";
            }
            else if (namatabel == 'oe')
            {
              var link = "../../back-end/print_report/print_report_oe_code.php";
              var jns = "OUTGOING";
              var jns2 = "EXTERNAL";
            }

            $.ajax({
              url:link,
              method:"POST",
              data:{
              	tgl_awal:tgl_awal,
              	tgl_akhir:tgl_akhir,
                thn:thn,
                jns:jns,
                jns2:jns2
              },
              success:function(data)
              {
                  $('#txt_select_report').val(null).trigger('change');
                  $('#txt_tgl_awal').val(null);
                  $('#txt_tgl_akhir').val(null);
                  $('#txt_thn_laporan').val(null);
              }
            });
  }
}