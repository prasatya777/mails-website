function importClick()
{
	var namatabel = $('#txt_tbl_import_excel').val();
	var file = $('#ImportExcelFile').val();
	
	if(namatabel != null || file != "")
	{
		if(namatabel == 'ii')
		{
			namatabel = 'Surat Incoming Internal';
		}
		else if(namatabel == 'ie')
		{
			namatabel = 'Surat Incoming External';
		}
		else if(namatabel == 'oi')
		{
			namatabel = 'Surat Outgoing Internal';
		}
		else if(namatabel == 'oe')
		{
			namatabel = 'Surat Outgoing External';
		}
		var app_extension = file.slice((file.lastIndexOf(".") - 1 >>> 0) + 2);

		if(app_extension != "xlsx")
		{
			swal("Error", "Jenis Ekstensi Yang Dapat Digunakan Adalah Microsoft Excel (.xlsx) !", "error");
		}

		else
		{
			$('#modalImport').modal('show');
			var isipesan = "Apakah Anda Yakin Import Excel Pada Tabel '"+namatabel+"' ";

			$("#modal-body-import").html(isipesan);
		}

	}
}

function jv_modal_import_excel()
{
	$('#ImportExcel').submit();
	$('#modalLoading').modal({
		keyboard: false,
		backdrop: 'static'
	});
		
	$('#modalLoading').modal('show');
}

// Export
function exportClick()
{

	var namatabel = $('#txt_tbl_export_excel').val();
	if(namatabel != null)
	{
		if(namatabel == 'ii')
		{
			var link_download = '../../back-end/import_download_excel/export_download_excel_ii_code.php';
		}
		else if(namatabel == 'ie')
		{
			var link_download = '../../back-end/import_download_excel/export_download_excel_ie_code.php';
		}
		else if(namatabel == 'oi')
		{
			var link_download = '../../back-end/import_download_excel/export_download_excel_oi_code.php';
		}
		else if(namatabel == 'oe')
		{
			var link_download = '../../back-end/import_download_excel/export_download_excel_oe_code.php';
		}
		$('#txt_tbl_export_excel').val(null).trigger('change');
		window.open(link_download, '_blank');

	}	
}

// Delete
function deleteClick()
{
	var namatabel = $('#txt_tbl_delete_excel').val();

	if(namatabel != null)
	{
		if(namatabel == 'ii')
		{
			namatabel = 'Surat Incoming Internal';
		}
		else if(namatabel == 'ie')
		{
			namatabel = 'Surat Incoming External';
		}
		else if(namatabel == 'oi')
		{
			namatabel = 'Surat Outgoing Internal';
		}
		else if(namatabel == 'oe')
		{
			namatabel = 'Surat Outgoing External';
		}

		$('#modalDeleteTruncate').modal('show');
		var isipesan = "Apakah Anda Yakin Menghapus Semua Data Yang Ada Pada Tabel '"+namatabel+"' ";

		$("#modal-body-deletetruncate").html(isipesan);
	}
}

function jv_modal_delete_excel()
{
			var tbl="";
            var namatabel = $('#txt_tbl_delete_excel').val();
            if (namatabel == 'ii')
            {
              tbl = "tbl_incoming_internal";
              namatabel = 'Surat Incoming Internal';
            }
            else if (namatabel == 'ie')
            {
              tbl = "tbl_incoming_external";
              namatabel = 'Surat Incoming External';
            }
            else if (namatabel == 'oi')
            {
              tbl = "tbl_outgoing_internal";
              namatabel = 'Surat Outgoing Internal';
            }
            else if (namatabel == 'oe')
            {
              tbl = "tbl_outgoing_external";
              namatabel = 'Surat Outgoing External';
            }
            event.preventDefault();
            
            $.ajax({
              url:"../../back-end/import_download_excel/delete_excel_code.php",
              method:"POST",
              data:{tbl:tbl},
              success:function(data)
              {
                if(data == "")
                {
                  $('#modalDeleteSucces').modal('show');

                  var hasil = "Hapus Data Table '"+namatabel+"' Berhasil";
                  $('#modal-body-delete-succes').html(hasil);

                  $('#txt_tbl_delete_excel').val(null).trigger('change');
                }
                else
                {
                  $('#modalDeleteSucces').modal('show');

                  var hasil = "Hapus Data Table '"+namatabel+"' Gagal !";
                  $('#modal-body-delete-success').html(hasil);

                  $('#txt_tbl_delete_excel').val(null).trigger('change');
                }
              }
            });
}