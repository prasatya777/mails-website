var iddelete;

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function submit_form_add()
{
	addAdminLogin();
 
}

function modalInsert_Click()
{
	$('#modalinsert').modal('show');
	$('#txt_no_pegawai').focus();
}

function addAdminLogin(){
	var txt_no_pegawai = $('#txt_no_pegawai').val();
	var txt_nama = $('#txt_nama').val();
	var txt_email = $('#txt_email').val();
	var txt_kelamin = $('#txt_kelamin').val();
	var txt_hakakses = $('#txt_hakakses').val();
	var txt_password = $('#txt_password').val();
	var txt_conf_password = $('#txt_conf_password').val();

	if(txt_no_pegawai == "")
	{
		swal("Information", "No Pegawai Tidak Boleh Kosong", "warning");
		$('#txt_no_pegawai').focus();
	}
	else if(txt_nama == "")
	{
		swal("Information", "Nama Lengkap Tidak Boleh Kosong", "warning");
		$('#txt_nama').focus();
	} 
	else if(txt_email =="")
	{
		swal("Information", "Email Tidak Boleh Kosong", "warning");
		$('#txt_email').focus();
	}
	else if (!validateEmail(txt_email))
	{
		swal("Information", "Format Email Tidak Sesuai", "warning");
		$('#txt_email').focus();
	}
	else if(txt_kelamin == null)
	{
		swal("Information", "Jenis Kelamin Tidak Boleh Kosong", "warning");
		$('#txt_kelamin').focus();
	}
	else if(txt_hakakses == null)
	{
		swal("Information", "Hak Akses Tidak Boleh Kosong", "warning");
		$('#txt_hakakses').focus();
	}
	else if(txt_password == "")
	{
		swal("Information", "Password Tidak Boleh Kosong", "warning");
		$('#txt_password').focus();
	}
	else if(txt_conf_password == "")
	{
		swal("Information", "Confirm Password Tidak Boleh Kosong", "warning");
		$('#txt_conf_password').focus();
	}
	else if(txt_password != txt_conf_password)
	{
		swal("Information", "Password Tidak Sesuai", "warning");
		$('#txt_password').val(null);
		$('#txt_conf_password').val(null);
		$('#txt_password').focus();
	}
	else
	{
		$.ajax({
			url:"../../back-end/admin_login/add_admin_login_code.php",
			type:'POST',
			data: { 
				txt_no_pegawai : txt_no_pegawai,
				txt_nama : txt_nama,
				txt_email : txt_email,
				txt_kelamin : txt_kelamin,
				txt_hakakses : txt_hakakses,
				txt_password : txt_password
			},

			success:function(data,status){
				showAdminLogin();
				$('#modalinsert').modal('hide');
	            $('#close-add').trigger('click');
	            resetAdminLogin();
	            $('#modalInsertSucces').modal('show');
			}

		});
	}

}

function showAdminLogin()
{
	$('#record_table').load('../../back-end/admin_login/show_admin_login_code.php');
	$('#java_script').load('../../page-admin/admin_login/js_admin_login.php');
}

function resetAdminLogin()
{
			$('#txt_no_pegawai').val(null);
			$('#txt_nama').val(null);
			$('#txt_email').val(null);
			$('#txt_kelamin').val(null);
			$('#txt_hakakses').val(null);
			$('#txt_password').val(null);
			$('#txt_conf_password').val(null);

			$('#txt_no_pegawai').focus();
}

function popupdeleteAdminLogin(id,txttext)
{
	$("#modalDelete").modal('show');

	iddelete = id;

	var isipesan = "Apakah Anda Yakin Menghapus User Dengan No Pegawai '"+id+"' Dan Nama User '"+txttext+"' ?";

	$("#modal-body-delete").html(isipesan);

}

function jv_modal_succes()
{
	setTimeout(function(){ $('#modalinsert').modal('show'); }, 1000);
}

function jv_modal_delete()
{
	var id = iddelete;
			$.ajax({
			url:"../../back-end/admin_login/delete_admin_login_code.php",
			type:"POST",
			data:{ id:id },
			success:function(data,status){
		   		showAdminLogin();
		   		$('#modalDeleteSucces').modal('show');
			}
		});
	iddelete = "";
}

// Bagian Edit-----------------------------------------------------------------------
function submit_form_edit()
{
	var no_pegawai = $('#txt_no_pegawai_update').val();
	swal({
	  title: "Edit Information",
	  text: "Apakah Anda Yakin Update Data User Dengan No Pegawai : '"+no_pegawai+"' ?",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	})
	.then((willDelete) => {
	  if (willDelete) 
	  {
	  	editAdminLogin();
	  } 
	});

}


function tampileditAdminLogin(id)
{
	$.post("../../back-end/admin_login/view_update_admin_login_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_al = JSON.parse(data);
				$('#txt_no_pegawai_update').val(view_update_al.no_pegawai_al);
				$('#txt_nama_update').val(view_update_al.nama_al);
				$('#txt_email_update').val(view_update_al.email_al);

				var select_jk = document.getElementById("txt_kelamin_update");
				select_jk.options.length = 0;

				if(view_update_al.jk_al == "p")
				{
					select_jk.options[select_jk.options.length] = new Option('Pria', 'p');
					select_jk.options[select_jk.options.length] = new Option('Wanita', 'w');
				}
				else
				{
					select_jk.options[select_jk.options.length] = new Option('Wanita', 'w');
					select_jk.options[select_jk.options.length] = new Option('Pria', 'p');
				}

				var select_ha = document.getElementById("txt_hakakses_update");
				select_ha.options.length = 0;

				if(view_update_al.hakakses_al == "admin")
				{
					select_ha.options[select_ha.options.length] = new Option('Admin', 'admin');
					select_ha.options[select_ha.options.length] = new Option('User', 'user');
				}
				else
				{
					select_ha.options[select_ha.options.length] = new Option('User', 'user');
					select_ha.options[select_ha.options.length] = new Option('Admin', 'admin');
				}

			});

	$('#modaledit').modal('show');
}

function editAdminLogin()
{
	var txt_no_pegawai = $('#txt_no_pegawai_update').val();
	var txt_nama = $('#txt_nama_update').val();
	var txt_email = $('#txt_email_update').val();
	var txt_kelamin = $('#txt_kelamin_update').val();
	var txt_hakakses = $('#txt_hakakses_update').val();
	var txt_password = $('#txt_password_update').val();
	var txt_conf_password = $('#txt_conf_password_update').val();

	if(txt_no_pegawai == "")
	{
		swal("Information", "No Pegawai Tidak Boleh Kosong", "warning");
		$('#txt_no_pegawai_update').focus();
	}
	else if(txt_nama == "")
	{
		swal("Information", "Nama Lengkap Tidak Boleh Kosong", "warning");
		$('#txt_nama_update').focus();
	} 
	else if(txt_email =="")
	{
		swal("Information", "Email Tidak Boleh Kosong", "warning");
		$('#txt_email_update').focus();
	}
	else if (!validateEmail(txt_email))
	{
		swal("Information", "Format Email Tidak Sesuai", "warning");
		$('#txt_email_update').focus();
	}
	else if(txt_kelamin == null)
	{
		swal("Information", "Jenis Kelamin Tidak Boleh Kosong", "warning");
		$('#txt_kelamin_update').focus();
	}
	else if(txt_hakakses == null)
	{
		swal("Information", "Hak Akses Tidak Boleh Kosong", "warning");
		$('#txt_hakakses_update').focus();
	}
	else if(txt_password == "")
	{
		swal("Information", "Password Tidak Boleh Kosong", "warning");
		$('#txt_password_update').focus();
	}
	else if(txt_conf_password == "")
	{
		swal("Information", "Confirm Password Tidak Boleh Kosong", "warning");
		$('#txt_conf_password_update').focus();
	}
	else if(txt_password != txt_conf_password)
	{
		swal("Information", "Password Tidak Sesuai", "warning");
		$('#txt_password_update').val(null);
		$('#txt_conf_password_update').val(null);
		$('#txt_password_update').focus();
	}

	else
	{

		$.ajax({
			url:"../../back-end/admin_login/update_admin_login_code.php",
			type:'POST',
			data: { 
				txt_no_pegawai : txt_no_pegawai,
				txt_nama : txt_nama,
				txt_email : txt_email,
				txt_kelamin : txt_kelamin,
				txt_hakakses : txt_hakakses,
				txt_password : txt_password
			},

			success:function(data,status){
				showAdminLogin();
				$('#modaledit').modal('hide');
	            $('#close-edit').trigger('click');
	            resetupdate_AdminLogin();
	            $('#modalEditSucces').modal('show');
			}

		});
	}
}


function resetupdate_AdminLogin()
{
			$('#txt_no_pegawai_update').val(null);
			$('#txt_nama_update').val(null);
			$('#txt_email_update').val(null);
			$('#txt_kelamin_update').val(null);
			$('#txt_hakakses_update').val(null);
			$('#txt_password_update').val(null);
			$('#txt_conf_password_update').val(null);

			$('#txt_nama_update').focus();
}