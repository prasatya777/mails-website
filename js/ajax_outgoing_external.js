var iddelete;

function modalInsert_Click()
{
	autonumberInternal();
	AddDropzone_Click();
	$('#modalinsert').modal('show');
	$('#txt_tglentry').focus();
}

function addOutgoingExternal(){
	var txt_nosurat = $('#txt_nosurat').val();
	var txt_tglentry = $('#txt_tglentry').val();
	var txt_nopeg = $('#txt_nopeg').val();
	var txt_kepada = $('#txt_kepada').val();
	var txt_prepared = $('#txt_prepared').val();
	var txt_subject	= $('#txt_subject').val();


	$.ajax({
		url:"../../back-end/outgoing_external/add_outgoing_external_code.php",
		type:'POST',
		data: { 
			txt_nosurat : txt_nosurat,
			txt_tglentry : txt_tglentry,
			txt_nopeg : txt_nopeg,
			txt_kepada : txt_kepada,
			txt_prepared : txt_prepared,
			txt_subject : txt_subject
		},

		success:function(data,status){
			showOutgoingExternal();
		}

	});

}

function autonumberInternal()
{
	$.ajax({
		url:"../../back-end/outgoing_external/auto_number_outgoing_external_code.php",
		success:function(data,status){
			$('#txt_nosurat').val(data);
		}
	});
	resetDate();
}

function resetDate()
{
	$.ajax({
		url:"../../back-end/outgoing_external/reset_date_outgoing_external_code.php",
		success:function(data,status){
			$('#txt_tglentry').val(data);
		}
	});
}

function showOutgoingExternal()
{
	$('#record_table').load('../../back-end/outgoing_external/show_outgoing_external_code.php');
	$('#java_script').load('../../page-admin/outgoing_internal/js_outgoing_internal.php');
}

function resetOutgoingExternal()
{
			autonumberInternal();
			$('#txt_tglentry').val(null);
			$('#txt_nopeg').val(null);
			$('#txt_kepada').val(null);
			$('#txt_prepared').val(null);
			$('#txt_subject').val(null);

			resetDate();
			$('#txt_tglentry').focus();
}

function popupdeleteOutgoingExternal(id,txttext)
{
	$("#modalDelete").modal('show');

	iddelete = id;

	var isipesan = "Apakah Anda Yakin Menghapus Surat Keluar External Dengan No Surat '"+id+"' Yang Ditujukan Kepada '"+txttext+"' ";

	$("#modal-body-delete").html(isipesan);

}

function jv_modal_succes()
{
	setTimeout(function(){ autonumberInternal(); $('#modalinsert').modal('show'); }, 1100);
}

function jv_modal_delete()
{
	var id = iddelete;
			$.ajax({
			url:"../../back-end/outgoing_external/delete_outgoing_external_code.php",
			type:"POST",
			data:{ id:id },
			success:function(data,status){
		   		showOutgoingExternal();
		   		$('#modalDeleteSucces').modal('show');
			}
		});
	iddelete = "";
}

// Bagian Edit-----------------------------------------------------------------------

function tampileditOutgoingExternal(id)
{
	EditDropzone_Click();
	$('#id_update_outgoing_external').val(id);

	$('#table_file_edit').load("../../back-end/outgoing_external/view_update_file_outgoing_external_code.php",{id:id});
	$('#java_script').load('../../page-admin/outgoing_internal/js_outgoing_internal.php');
	

	$.post("../../back-end/outgoing_external/view_update_outgoing_external_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_oe = JSON.parse(data);
				$('#txt_nosurat_update').val(view_update_oe.no_surat_oe);
				$('#txt_tglentry_update').val(view_update_oe.tgl_oe);
				$('#txt_nopeg_update').val(view_update_oe.nopeg_oe);
				$('#txt_kepada_update').val(view_update_oe.kepada_oe);
				$('#txt_prepared_update').val(view_update_oe.prepared_by_oe);
				$('#txt_subject_update').val(view_update_oe.subject_oe);

			});

	$('#modaledit').modal('show');
}

function editOutgoingExternal()
{
	var txt_nosurat = $('#txt_nosurat_update').val();
	var txt_tglentry = $('#txt_tglentry_update').val();
	var txt_nopeg = $('#txt_nopeg_update').val();
	var txt_kepada = $('#txt_kepada_update').val();
	var txt_prepared = $('#txt_prepared_update').val();
	var txt_subject	= $('#txt_subject_update').val();

	$.ajax({
		url:"../../back-end/outgoing_external/update_outgoing_external_code.php",
		type:'POST',
		data: { 
			txt_nosurat : txt_nosurat,
			txt_tglentry : txt_tglentry,
			txt_nopeg : txt_nopeg,
			txt_kepada : txt_kepada,
			txt_prepared : txt_prepared,
			txt_subject : txt_subject
		},

		success:function(data,status){
			showOutgoingExternal();
		}

	});
}


function resetupdate_OutgoingExternal()
{
			$('#txt_tglentry').val(null);
			$('#txt_nopeg').val(null);
			$('#txt_kepada').val(null);
			$('#txt_prepared').val(null);
			$('#txt_subject').val(null);

			resetDate();
			$('#txt_tglentry').focus();
}

// Bagian View & Download

function viewdownloadOutgoingExternal(id)
{

	$('#table_file_view').load("../../back-end/outgoing_external/view_download_file_outgoing_external_code.php",{id:id});
	$('#java_script').load('../../page-admin/outgoing_internal/js_outgoing_internal.php');

	$.post("../../back-end/outgoing_external/view_update_outgoing_external_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_oe = JSON.parse(data);
				$('#txt_nosurat_view').val(view_update_oe.no_surat_oe);
				$('#txt_tglentry_view').val(view_update_oe.tgl_oe);
				$('#txt_nopeg_view').val(view_update_oe.nopeg_oe);
				$('#txt_kepada_view').val(view_update_oe.kepada_oe);
				$('#txt_prepared_view').val(view_update_oe.prepared_by_oe);
				$('#txt_subject_view').val(view_update_oe.subject_oe);

			});

	$('#modalviewdownload').modal('show');
}

function downloadOutgoingExternal(id_ag,id_file)
{
	$.post("../../back-end/outgoing_external/download_file_outgoing_external_code.php", 
		{
			id_ag:id_ag,
			id_file:id_file
		}, 
		function(data,status)
			{

			});
}

// Bagian Delete -------------------------------------------------------------
function deletefileeditOutgoingExternal(id_ag,id_file,file_name)
{
    swal({
            title: "Delete Information",
            text: "Apakah Anda Yakin Menghapus File : '"+file_name+"' Dengan No Surat : '"+id_ag+"' ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) 
            {
				$.ajax({
					url:"../../back-end/outgoing_external/delete_file_outgoing_external_code.php",
					type:"POST",
					data:{ 
						id_ag:id_ag,
						id_file:id_file
						},
					success:function(data,status){
						swal("Information", "Delete File Berhasil", "success");
						$('#table_file_edit').load("../../back-end/outgoing_external/view_update_file_outgoing_external_code.php",{id:id_ag});
						$('#java_script').load('../../page-admin/outgoing_internal/js_outgoing_internal.php');
					}
				});
            } 
        });
}