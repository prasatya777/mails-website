function removeallfile_Dropzone()
{
      $('#txt_nosurat').val(null);
      $('#txt_pengirim').val(null);
      $('#txt_status').val(null);
      $('#txt_perihal').val(null);
      $('#txt_disposisi_direksi').val(null);
      $('#txt_tembusan').val(null);
      $('#txt_disposisi_id').val(null);
      $('#txt_isi_disposisi_id').val(null);
      resetDate();
  var count_file = myDropzone.files.length;
  if (count_file > 0)
  {
    myDropzone.removeAllFiles(true);
    myDropzone.removeAllFiles(false);
  }

}

function AddDropzone_Click(){
  var Dropzone1 = $("#add_dropzone")[0].dropzone;
  var Dropzone2 = $("#edit_dropzone")[0].dropzone;

if ( Dropzone1 || Dropzone2) 
  {
    myDropzone.destroy();
    AddDropzone_Set();
  }
else
  {
    AddDropzone_Set();
  }


}

function AddDropzone_Set()
{
  var add_dropzone_v = new Dropzone(
        '#add_dropzone',
        {           
          init: function() {
            var submitButtonadd = document.querySelector("#submit-incoming-internal") 
                myDropzone = this; // closure

          var addII =  function(file) {
              var count_file = myDropzone.files.length;
              addIncomingInternal();

              if(count_file > 0)
              {
                myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                myDropzone.options.autoProcessQueue = true;
                submitButtonadd.removeEventListener("click", addII);
              }
              else
              {
                resetIncomingInternal();
                $('#modalinsert').modal('hide');
                $('#close-add').trigger('click');
                $('#modalInsertSucces').modal('show');
                submitButtonadd.removeEventListener("click", addII);
                AddDropzone_Click();
              }


            }
            submitButtonadd.addEventListener("click", addII);

            this.on("complete", function(file) {
                  myDropzone.removeFile(file);
            });

            this.on("queuecomplete", function(file) {
                  myDropzone.options.autoProcessQueue = false;
                  myDropzone.disable();
                  myDropzone.enable();
                  resetIncomingInternal();
                  $('#modalinsert').modal('hide');
                  $('#close-add').trigger('click');
                  $('#modalInsertSucces').modal('show');
                  AddDropzone_Click();
            });

            this.on("error", function(file) {
              swal("Error", "Maaf Sebagian File Tidak Dapat Diupload (File MaxSize : 2MB & Total MaxFile : 10 File)", "error");
              myDropzone.removeFile(file);
            });

            this.on("canceled", function(file) {
              swal("Error", "Maaf Sebagian File Gagal Diupload !", "error");
              myDropzone.removeFile(file);
            });

          }
        });
}

function EditDropzone_Click(){

  var Dropzone1 = $("#add_dropzone")[0].dropzone;
  var Dropzone2 = $("#edit_dropzone")[0].dropzone;

  if ( Dropzone1 || Dropzone2) 
    {
      myDropzone.destroy();
      EditDropzone_Set();
    }
  else
    {
      EditDropzone_Set();
    }

}

function EditDropzone_Set()
{
  var edit_dropzone_v = new Dropzone(
        '#edit_dropzone',
        {           
          init: function() {
            var submitButton = document.querySelector("#edit-incoming-internal")
                myDropzone = this; // closure
            var editII = function(file) {
              var noagenda = $('#txt_noagenda_update').val();
              swal({
                title: "Edit Information",
                text: "Apakah Anda Yakin Update Data Dengan No Agenda : '"+noagenda+"' ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) 
                {
                  var count_file = myDropzone.files.length;
                  editIncomingInternal();

                  if(count_file > 0)
                  {
                    myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                    myDropzone.options.autoProcessQueue = true;
                    submitButton.removeEventListener("click", editII);
                  }
                  else
                  {
                    resetupdate_IncomingInternal();
                    $('#modaledit').modal('hide');
                    $('#close-edit').trigger('click');
                    $('#modalEditSucces').modal('show');
                    submitButton.removeEventListener("click", editII);
                  }
                } 
              });
            }

            submitButton.addEventListener("click", editII);

            this.on("complete", function(file) {
                  myDropzone.removeFile(file);
            });

            this.on("queuecomplete", function(file) {
                  myDropzone.options.autoProcessQueue = false;
                  myDropzone.disable();
                  myDropzone.enable();
                  resetupdate_IncomingInternal();
                  $('#modaledit').modal('hide');
                  $('#close-edit').trigger('click');
                  $('#modalEditSucces').modal('show');
            });

            this.on("error", function(file) {
              swal("Error", "Maaf Sebagian File Tidak Dapat Diupload (File MaxSize : 2MB & Total MaxFile : 10 File)", "error");
              myDropzone.removeFile(file);
            });

            this.on("canceled", function(file) {
              swal("Error", "Maaf Sebagian File Gagal Diupload !", "error");
              myDropzone.removeFile(file);
            });

          }
        });
}
