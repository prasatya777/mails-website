var iddelete;

function modalInsert_Click()
{
	autonumberExternal();
	AddDropzone_Click();
	$('#modalinsert').modal('show');
	$('#txt_nosurat').focus();
}

function addIncomingExternal(){
	var txt_noagenda = $('#txt_noagenda').val();
	var txt_nosurat = $('#txt_nosurat').val();
	var txt_tglentry = $('#txt_tglentry').val();
	var txt_pengirim = $('#txt_pengirim').val();
	var txt_status	= $('#txt_status').val();
	var txt_perihal	= $('#txt_perihal').val();
	var txt_disposisi_direksi = $('#txt_disposisi_direksi').val();
	var txt_tembusan = $('#txt_tembusan').val();
	var txt_disposisi_id = $('#txt_disposisi_id').val();
	var txt_tgl_disposisi_id = $('#txt_tgl_disposisi_id').val();
	var txt_isi_disposisi_id = $('#txt_isi_disposisi_id').val();

	$.ajax({
		url:"../../back-end/incoming_external/add_incoming_external_code.php",
		type:'POST',
		data: { txt_noagenda : txt_noagenda,
			txt_nosurat : txt_nosurat,
			txt_tglentry : txt_tglentry,
			txt_pengirim : txt_pengirim,
			txt_status : txt_status,
			txt_perihal : txt_perihal,
			txt_disposisi_direksi : txt_disposisi_direksi,
			txt_tembusan : txt_tembusan,
			txt_disposisi_id : txt_disposisi_id,
			txt_tgl_disposisi_id : txt_tgl_disposisi_id,
			txt_isi_disposisi_id : txt_isi_disposisi_id
		},

		success:function(data,status){
			showIncomingExternal();
		}

	});

}

function autonumberExternal()
{
	$.ajax({
		url:"../../back-end/incoming_external/auto_number_incoming_external_code.php",
		success:function(data,status){
			$('#txt_noagenda').val(data);
		}
	});
	resetDate();
}

function resetDate()
{
	$.ajax({
		url:"../../back-end/incoming_external/reset_date_incoming_external_code.php",
		success:function(data,status){
			$('#txt_tgl_disposisi_id').val(data);
			$('#txt_tglentry').val(data);
		}
	});
}

function showIncomingExternal()
{
	$('#record_table').load('../../back-end/incoming_external/show_incoming_external_code.php');
	$('#java_script').load('../../page-admin/incoming_external/js_incoming_external.php');
}

function resetIncomingExternal()
{
			autonumberExternal();
			$('#txt_nosurat').val(null);
			$('#txt_pengirim').val(null);
			$('#txt_status').val(null);
			$('#txt_perihal').val(null);
			$('#txt_disposisi_direksi').val(null);
			$('#txt_tembusan').val(null);
			$('#txt_disposisi_id').val(null);
			$('#txt_isi_disposisi_id').val(null);
			resetDate();
			$('#txt_nosurat').focus();
}

function popupdeleteIncomingExternal(id,txttext)
{
	$("#modalDelete").modal('show');

	iddelete = id;

	var isipesan = "Apakah Anda Yakin Menghapus Surat Masuk External Dengan No Agenda '"+id+"' Dan No Surat '"+txttext+"' ";

	$("#modal-body-delete").html(isipesan);

}

function jv_modal_succes()
{
	setTimeout(function(){ autonumberExternal(); $('#modalinsert').modal('show'); }, 1100);
}

function jv_modal_delete()
{
	var id = iddelete;
			$.ajax({
			url:"../../back-end/incoming_external/delete_incoming_external_code.php",
			type:"POST",
			data:{ id:id },
			success:function(data,status){
		   		showIncomingExternal();
		   		$('#modalDeleteSucces').modal('show');
			}
		});
	iddelete = "";
}

// Bagian Edit-----------------------------------------------------------------------

function tampileditIncomingExternal(id)
{
	EditDropzone_Click();
	$('#id_update_incoming_external').val(id);

	$('#table_file_edit').load("../../back-end/incoming_external/view_update_file_incoming_external_code.php",{id:id});
	$('#java_script').load('../../page-admin/incoming_external/js_incoming_external.php');
	

	$.post("../../back-end/incoming_external/view_update_incoming_external_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_ie = JSON.parse(data);
				$('#txt_noagenda_update').val(view_update_ie.no_agenda_ie);
				$('#txt_nosurat_update').val(view_update_ie.no_surat_ie);
				$('#txt_tglentry_update').val(view_update_ie.tgl_entry_ie);
				$('#txt_pengirim_update').val(view_update_ie.pengirim_ie);
				$('#txt_status_update').val(view_update_ie.status_ie);
				$('#txt_perihal_update').val(view_update_ie.perihal_ie);
				$('#txt_disposisi_direksi_update').val(view_update_ie.disposisi_direksi_ie);
				$('#txt_tembusan_update').val(view_update_ie.tembusan_ie);
				$('#txt_disposisi_id_update').val(view_update_ie.disposisi_id);
				$('#txt_tgl_disposisi_id_update').val(view_update_ie.tgl_disposisi_id_ie);
				$('#txt_isi_disposisi_id_update').val(view_update_ie.isi_disposisi_ie);

			});

	$('#modaledit').modal('show');
}

function editIncomingExternal()
{
	var txt_noagenda = $('#txt_noagenda_update').val();
	var txt_nosurat = $('#txt_nosurat_update').val();
	var txt_tglentry = $('#txt_tglentry_update').val();
	var txt_pengirim = $('#txt_pengirim_update').val();
	var txt_status	= $('#txt_status_update').val();
	var txt_perihal	= $('#txt_perihal_update').val();
	var txt_disposisi_direksi = $('#txt_disposisi_direksi_update').val();
	var txt_tembusan = $('#txt_tembusan_update').val();
	var txt_disposisi_id = $('#txt_disposisi_id_update').val();
	var txt_tgl_disposisi_id = $('#txt_tgl_disposisi_id_update').val();
	var txt_isi_disposisi_id = $('#txt_isi_disposisi_id_update').val();

	$.ajax({
		url:"../../back-end/incoming_external/update_incoming_external_code.php",
		type:'POST',
		data: { txt_noagenda : txt_noagenda,
			txt_nosurat : txt_nosurat,
			txt_tglentry : txt_tglentry,
			txt_pengirim : txt_pengirim,
			txt_status : txt_status,
			txt_perihal : txt_perihal,
			txt_disposisi_direksi : txt_disposisi_direksi,
			txt_tembusan : txt_tembusan,
			txt_disposisi_id : txt_disposisi_id,
			txt_tgl_disposisi_id : txt_tgl_disposisi_id,
			txt_isi_disposisi_id : txt_isi_disposisi_id
		},

		success:function(data,status){
			showIncomingExternal();
		}

	});
}


function resetupdate_IncomingExternal()
{
			$('#txt_nosurat').val(null);
			$('#txt_pengirim').val(null);
			$('#txt_status').val(null);
			$('#txt_perihal').val(null);
			$('#txt_disposisi_direksi').val(null);
			$('#txt_tembusan').val(null);
			$('#txt_disposisi_id').val(null);
			$('#txt_isi_disposisi_id').val(null);
			resetDate();
			$('#txt_nosurat').focus();
}

// Bagian View & Download

function viewdownloadIncomingExternal(id)
{

	$('#table_file_view').load("../../back-end/incoming_external/view_download_file_incoming_external_code.php",{id:id});
	$('#java_script').load('../../page-admin/incoming_external/js_incoming_external.php');

	$.post("../../back-end/incoming_external/view_update_incoming_external_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_ie = JSON.parse(data);
				$('#txt_noagenda_view').val(view_update_ie.no_agenda_ie);
				$('#txt_nosurat_view').val(view_update_ie.no_surat_ie);
				$('#txt_tglentry_view').val(view_update_ie.tgl_entry_ie);
				$('#txt_pengirim_view').val(view_update_ie.pengirim_ie);
				$('#txt_status_view').val(view_update_ie.status_ie);
				$('#txt_perihal_view').val(view_update_ie.perihal_ie);
				$('#txt_disposisi_direksi_view').val(view_update_ie.disposisi_direksi_ie);
				$('#txt_tembusan_view').val(view_update_ie.tembusan_ie);
				$('#txt_disposisi_id_view').val(view_update_ie.disposisi_id);
				$('#txt_tgl_disposisi_id_view').val(view_update_ie.tgl_disposisi_id_ie);
				$('#txt_isi_disposisi_id_view').val(view_update_ie.isi_disposisi_ie);

			});

	$('#modalviewdownload').modal('show');
}

function downloadIncomingExternal(id_ag,id_file)
{

	$.post("../../back-end/incoming_external/download_file_incoming_external_code.php", 
		{
			id_ag:id_ag,
			id_file:id_file
		}, 
		function(data,status)
			{

			});
}

// Bagian Delete -------------------------------------------------------------
function deletefileeditIncomingExternal(id_ag,id_file,file_name)
{
    swal({
            title: "Delete Information",
            text: "Apakah Anda Yakin Menghapus File : '"+file_name+"' Dengan No Agenda : '"+id_ag+"' ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) 
            {

				$.ajax({
					url:"../../back-end/incoming_external/delete_file_incoming_external_code.php",
					type:"POST",
					data:{ 
						id_ag:id_ag,
						id_file:id_file
						},
					success:function(data,status){
						swal("Information", "Delete File Berhasil", "success");
						$('#table_file_edit').load("../../back-end/incoming_external/view_update_file_incoming_external_code.php",{id:id_ag});
						$('#java_script').load('../../page-admin/incoming_external/js_incoming_external.php');
					}
				});
            } 
        });
}