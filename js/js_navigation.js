function nav_home()
{
	window.location.href='../../page-admin/home/dashboard_home.php';
}

function nav_user()
{
	window.location.href='../../page-admin/admin_login/dashboard_admin_login.php';
}

function nav_ii()
{
	window.location.href='../../page-admin/incoming_internal/dashboard_incoming_internal.php';
}

function nav_ie()
{
	window.location.href='../../page-admin/incoming_external/dashboard_incoming_external.php';
}

function nav_oi()
{
	window.location.href='../../page-admin/outgoing_internal/dashboard_outgoing_internal.php';
}

function nav_oe()
{
	window.location.href='../../page-admin/outgoing_external/dashboard_outgoing_external.php';
}

function nav_print()
{
	window.location.href='../../page-admin/print_report/dashboard_print_report.php';
}

function nav_exp()
{
	window.location.href='../../page-admin/import_download_excel/dashboard_import_download_excel.php';
}

