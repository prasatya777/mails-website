function validateEmailLogin(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function shownamaLogin()
{
	$.post("../../back-end/public_user/show_nama.php", {}, 
		function(data)
			{
				$("#WelcomeUser").html(data);
			});
}

// Update Login

function getnopegLogin()
{
	$.post("../../back-end/public_user/show_nopeg.php", {}, 
		function(data)
			{
				tampiledituserLogin(data);
			});

}

function tampiledituserLogin(id)
{
	$.post("../../back-end/public_user/view_update_publicuser_code.php", {id:id}, 
		function(data)
			{
				var view_update_al = JSON.parse(data);
				$('#txt_no_pegawai_login').val(view_update_al.no_pegawai_al);
				$('#txt_nama_login').val(view_update_al.nama_al);
				$('#txt_email_login').val(view_update_al.email_al);

				var select_jk = document.getElementById("txt_kelamin_login");
				select_jk.options.length = 0;

				if(view_update_al.jk_al == "p")
				{
					select_jk.options[select_jk.options.length] = new Option('Pria', 'p');
					select_jk.options[select_jk.options.length] = new Option('Wanita', 'w');
				}
				else
				{
					select_jk.options[select_jk.options.length] = new Option('Wanita', 'w');
					select_jk.options[select_jk.options.length] = new Option('Pria', 'p');
				}

				var select_ha = document.getElementById("txt_hakakses_login");
				select_ha.options.length = 0;

				if(view_update_al.hakakses_al == "admin")
				{
					select_ha.options[select_ha.options.length] = new Option('Admin', 'admin');
				}
				else
				{
					select_ha.options[select_ha.options.length] = new Option('User', 'user');
				}

			});
	$('#modaledituserNavbar').modal('show');
}

function submit_editLogin()
{
	var no_pegawai = $('#txt_no_pegawai_login').val();
	swal({
	  title: "Edit Information",
	  text: "Apakah Anda Yakin Update Data User Dengan No Pegawai : '"+no_pegawai+"' ?",
	  icon: "warning",
	  buttons: true,
	  dangerMode: true,
	})
	.then((willDelete) => {
	  if (willDelete) 
	  {
	  	edituserLogin();
	  } 
	});
}

function edituserLogin()
{
	var txt_no_pegawai = $('#txt_no_pegawai_login').val();
	var txt_nama = $('#txt_nama_login').val();
	var txt_email = $('#txt_email_login').val();
	var txt_kelamin = $('#txt_kelamin_login').val();
	var txt_hakakses = $('#txt_hakakses_login').val();
	var txt_password = $('#txt_password_login').val();
	var txt_conf_password = $('#txt_conf_password_login').val();

	if(txt_no_pegawai == "")
	{
		swal("Information", "No Pegawai Tidak Boleh Kosong", "warning");
		$('#txt_no_pegawai_login').focus();
	}
	else if(txt_nama == "")
	{
		swal("Information", "Nama Lengkap Tidak Boleh Kosong", "warning");
		$('#txt_nama_login').focus();
	} 
	else if(txt_email =="")
	{
		swal("Information", "Email Tidak Boleh Kosong", "warning");
		$('#txt_email_login').focus();
	}
	else if (!validateEmailLogin(txt_email))
	{
		swal("Information", "Format Email Tidak Sesuai", "warning");
		$('#txt_email_login').focus();
	}
	else if(txt_kelamin == null)
	{
		swal("Information", "Jenis Kelamin Tidak Boleh Kosong", "warning");
		$('#txt_kelamin_login').focus();
	}
	else if(txt_hakakses == null)
	{
		swal("Information", "Hak Akses Tidak Boleh Kosong", "warning");
		$('#txt_hakakses_login').focus();
	}
	else if(txt_password == "")
	{
		swal("Information", "Password Tidak Boleh Kosong", "warning");
		$('#txt_password_login').focus();
	}
	else if(txt_conf_password == "")
	{
		swal("Information", "Confirm Password Tidak Boleh Kosong", "warning");
		$('#txt_conf_password_login').focus();
	}
	else if(txt_password != txt_conf_password)
	{
		swal("Information", "Password Tidak Sesuai", "warning");
		$('#txt_password_login').val(null);
		$('#txt_conf_password_login').val(null);
		$('#txt_password_login').focus();
	}
	else
	{
		$.ajax({
			url:"../../back-end/public_user/update_publicuser_code.php",
			type:'POST',
			data: { 
				txt_no_pegawai : txt_no_pegawai,
				txt_nama : txt_nama,
				txt_email : txt_email,
				txt_kelamin : txt_kelamin,
				txt_hakakses : txt_hakakses,
				txt_password : txt_password
			},

			success:function(data){
				$('#modaledituserNavbar').modal('hide');
	            $('#close-edituserNavbar').trigger('click');
	            resetupdateuserLogin();
	            $('#modalEditSuccesLogin').modal('show');
	            shownamaLogin();
			}

		});
	}
}

function resetupdateuserLogin()
{
			$('#txt_no_pegawai_login').val(null);
			$('#txt_nama_login').val(null);
			$('#txt_email_login').val(null);
			$('#txt_kelamin_login').val(null);
			$('#txt_hakakses_login').val(null);
			$('#txt_password_login').val(null);
			$('#txt_conf_password_login').val(null);

			$('#txt_nama').focus();
}

// Logout
function submitLogout()
{
	$.post("../../back-end/public_user/logout_publicuser.php", {}, 
		function(data)
		{
			window.location.href='../../login/login.php';
		});
}

$(document).ready(function() {
    shownamaLogin();
});