var iddelete;

function modalInsert_Click()
{
	autonumberInternal();
	AddDropzone_Click();
	$('#modalinsert').modal('show');
	$('#txt_tglentry').focus();
}

function addOutgoingInternal(){
	var txt_nosurat = $('#txt_nosurat').val();
	var txt_tglentry = $('#txt_tglentry').val();
	var txt_nopeg = $('#txt_nopeg').val();
	var txt_kepada = $('#txt_kepada').val();
	var txt_prepared = $('#txt_prepared').val();
	var txt_subject	= $('#txt_subject').val();
	var txt_tambahan = $('#txt_tambahan').val();


	$.ajax({
		url:"../../back-end/outgoing_internal/add_outgoing_internal_code.php",
		type:'POST',
		data: { 
			txt_nosurat : txt_nosurat,
			txt_tglentry : txt_tglentry,
			txt_nopeg : txt_nopeg,
			txt_kepada : txt_kepada,
			txt_prepared : txt_prepared,
			txt_subject : txt_subject,
			txt_tambahan : txt_tambahan
		},

		success:function(data,status){
			showOutgoingInternal();
		}

	});

}

function autonumberInternal()
{
	$.ajax({
		url:"../../back-end/outgoing_internal/auto_number_outgoing_internal_code.php",
		success:function(data,status){
			$('#txt_nosurat').val(data);
		}
	});
	resetDate();
}

function resetDate()
{
	$.ajax({
		url:"../../back-end/outgoing_internal/reset_date_outgoing_internal_code.php",
		success:function(data,status){
			$('#txt_tglentry').val(data);
		}
	});
}

function showOutgoingInternal()
{
	$('#record_table').load('../../back-end/outgoing_internal/show_outgoing_internal_code.php');
	$('#java_script').load('../../page-admin/outgoing_external/js_outgoing_external.php');
}

function resetOutgoingInternal()
{
			autonumberInternal();
			$('#txt_tglentry').val(null);
			$('#txt_nopeg').val(null);
			$('#txt_kepada').val(null);
			$('#txt_prepared').val(null);
			$('#txt_subject').val(null);
			$('#txt_tambahan').val(null);

			resetDate();
			$('#txt_tglentry').focus();
}

function popupdeleteOutgoingInternal(id,txttext)
{
	$("#modalDelete").modal('show');

	iddelete = id;

	var isipesan = "Apakah Anda Yakin Menghapus Surat Keluar Internal Dengan No Surat '"+id+"' Yang Ditujukan Kepada '"+txttext+"' ";

	$("#modal-body-delete").html(isipesan);

}

function jv_modal_succes()
{
	setTimeout(function(){ autonumberInternal(); $('#modalinsert').modal('show'); }, 1100);
}

function jv_modal_delete()
{
	var id = iddelete;
			$.ajax({
			url:"../../back-end/outgoing_internal/delete_outgoing_internal_code.php",
			type:"POST",
			data:{ id:id },
			success:function(data,status){
		   		showOutgoingInternal();
		   		$('#modalDeleteSucces').modal('show');
			}
		});
	iddelete = "";
}

// Bagian Edit-----------------------------------------------------------------------

function tampileditOutgoingInternal(id)
{
	EditDropzone_Click();
	$('#id_update_outgoing_internal').val(id);

	$('#table_file_edit').load("../../back-end/outgoing_internal/view_update_file_outgoing_internal_code.php",{id:id});
	$('#java_script').load('../../page-admin/outgoing_external/js_outgoing_external.php');
	

	$.post("../../back-end/outgoing_internal/view_update_outgoing_internal_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_oi = JSON.parse(data);
				$('#txt_nosurat_update').val(view_update_oi.no_surat_oi);
				$('#txt_tglentry_update').val(view_update_oi.tgl_oi);
				$('#txt_nopeg_update').val(view_update_oi.nopeg_oi);
				$('#txt_kepada_update').val(view_update_oi.kepada_oi);
				$('#txt_prepared_update').val(view_update_oi.prepared_by_oi);
				$('#txt_subject_update').val(view_update_oi.subject_oi);
				$('#txt_tambahan_update').val(view_update_oi.keterangan_oi);

			});

	$('#modaledit').modal('show');
}

function editOutgoingInternal()
{
	var txt_nosurat = $('#txt_nosurat_update').val();
	var txt_tglentry = $('#txt_tglentry_update').val();
	var txt_nopeg = $('#txt_nopeg_update').val();
	var txt_kepada = $('#txt_kepada_update').val();
	var txt_prepared = $('#txt_prepared_update').val();
	var txt_subject	= $('#txt_subject_update').val();
	var txt_tambahan = $('#txt_tambahan_update').val();

	$.ajax({
		url:"../../back-end/outgoing_internal/update_outgoing_internal_code.php",
		type:'POST',
		data: { 
			txt_nosurat : txt_nosurat,
			txt_tglentry : txt_tglentry,
			txt_nopeg : txt_nopeg,
			txt_kepada : txt_kepada,
			txt_prepared : txt_prepared,
			txt_subject : txt_subject,
			txt_tambahan : txt_tambahan
		},

		success:function(data,status){
			showOutgoingInternal();
		}

	});
}


function resetupdate_OutgoingInternal()
{
			$('#txt_tglentry').val(null);
			$('#txt_nopeg').val(null);
			$('#txt_kepada').val(null);
			$('#txt_prepared').val(null);
			$('#txt_subject').val(null);
			$('#txt_tambahan').val(null);
			resetDate();
			$('#txt_tglentry').focus();
}

// Bagian View & Download

function viewdownloadOutgoingInternal(id)
{

	$('#table_file_view').load("../../back-end/outgoing_internal/view_download_file_outgoing_internal_code.php",{id:id});
	$('#java_script').load('../../page-admin/outgoing_external/js_outgoing_external.php');

	$.post("../../back-end/outgoing_internal/view_update_outgoing_internal_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_oi = JSON.parse(data);
				$('#txt_nosurat_view').val(view_update_oi.no_surat_oi);
				$('#txt_tglentry_view').val(view_update_oi.tgl_oi);
				$('#txt_nopeg_view').val(view_update_oi.nopeg_oi);
				$('#txt_kepada_view').val(view_update_oi.kepada_oi);
				$('#txt_prepared_view').val(view_update_oi.prepared_by_oi);
				$('#txt_subject_view').val(view_update_oi.subject_oi);
				$('#txt_tambahan_view').val(view_update_oi.keterangan_oi);

			});

	$('#modalviewdownload').modal('show');
}

function downloadOutgoingInternal(id_ag,id_file)
{

	$.post("../../back-end/outgoing_internal/download_file_outgoing_internal_code.php", 
		{
			id_ag:id_ag,
			id_file:id_file
		}, 
		function(data,status)
			{

			});
}

// Bagian Delete -------------------------------------------------------------
function deletefileeditOutgoingInternal(id_ag,id_file,file_name)
{

    swal({
            title: "Delete Information",
            text: "Apakah Anda Yakin Menghapus File : '"+file_name+"' Dengan No Surat : '"+id_ag+"' ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) 
            {
				$.ajax({
					url:"../../back-end/outgoing_internal/delete_file_outgoing_internal_code.php",
					type:"POST",
					data:{ 
						id_ag:id_ag,
						id_file:id_file
						},
					success:function(data,status){
						swal("Information", "Delete File Berhasil", "success");
						$('#table_file_edit').load("../../back-end/outgoing_internal/view_update_file_outgoing_internal_code.php",{id:id_ag});
						$('#java_script').load('../../page-admin/outgoing_external/js_outgoing_external.php');
					}
				});
            } 
        });
}