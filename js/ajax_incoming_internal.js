var iddelete;

function modalInsert_Click()
{
	autonumberInternal();
	AddDropzone_Click();
	$('#modalinsert').modal('show');
	$('#txt_nosurat').focus();
}

function addIncomingInternal(){
	var txt_noagenda = $('#txt_noagenda').val();
	var txt_nosurat = $('#txt_nosurat').val();
	var txt_tglentry = $('#txt_tglentry').val();
	var txt_pengirim = $('#txt_pengirim').val();
	var txt_status	= $('#txt_status').val();
	var txt_perihal	= $('#txt_perihal').val();
	var txt_disposisi_direksi = $('#txt_disposisi_direksi').val();
	var txt_tembusan = $('#txt_tembusan').val();
	var txt_disposisi_id = $('#txt_disposisi_id').val();
	var txt_tgl_disposisi_id = $('#txt_tgl_disposisi_id').val();
	var txt_isi_disposisi_id = $('#txt_isi_disposisi_id').val();

	$.ajax({
		url:"../../back-end/incoming_internal/add_incoming_internal_code.php",
		type:'POST',
		data: { txt_noagenda : txt_noagenda,
			txt_nosurat : txt_nosurat,
			txt_tglentry : txt_tglentry,
			txt_pengirim : txt_pengirim,
			txt_status : txt_status,
			txt_perihal : txt_perihal,
			txt_disposisi_direksi : txt_disposisi_direksi,
			txt_tembusan : txt_tembusan,
			txt_disposisi_id : txt_disposisi_id,
			txt_tgl_disposisi_id : txt_tgl_disposisi_id,
			txt_isi_disposisi_id : txt_isi_disposisi_id
		},

		success:function(data,status){
			showIncomingInternal();
		}

	});

}

function autonumberInternal()
{
	$.ajax({
		url:"../../back-end/incoming_internal/auto_number_incoming_internal_code.php",
		success:function(data,status){
			$('#txt_noagenda').val(data);
		}
	});
	resetDate();
}

function resetDate()
{
	$.ajax({
		url:"../../back-end/incoming_internal/reset_date_incoming_internal_code.php",
		success:function(data,status){
			$('#txt_tgl_disposisi_id').val(data);
			$('#txt_tglentry').val(data);
		}
	});
}

function showIncomingInternal()
{
	$('#record_table').load('../../back-end/incoming_internal/show_incoming_internal_code.php');
	$('#java_script').load('../../page-admin/incoming_internal/js_incoming_internal.php');
}

function resetIncomingInternal()
{
			autonumberInternal();
			$('#txt_nosurat').val(null);
			$('#txt_pengirim').val(null);
			$('#txt_status').val(null);
			$('#txt_perihal').val(null);
			$('#txt_disposisi_direksi').val(null);
			$('#txt_tembusan').val(null);
			$('#txt_disposisi_id').val(null);
			$('#txt_isi_disposisi_id').val(null);
			resetDate();
			$('#txt_nosurat').focus();
}

function popupdeleteIncomingInternal(id,txttext)
{
	$("#modalDelete").modal('show');

	iddelete = id;

	var isipesan = "Apakah Anda Yakin Menghapus Surat Masuk Internal Dengan No Agenda '"+id+"' Dan No Surat '"+txttext+"' ";

	$("#modal-body-delete").html(isipesan);

}

function jv_modal_succes()
{
	setTimeout(function(){ autonumberInternal(); $('#modalinsert').modal('show'); }, 1100);
}

function jv_modal_delete()
{
	var id = iddelete;
			$.ajax({
			url:"../../back-end/incoming_internal/delete_incoming_internal_code.php",
			type:"POST",
			data:{ id:id },
			success:function(data,status){
		   		showIncomingInternal();
		   		$('#modalDeleteSucces').modal('show');
			}
		});
	iddelete = "";
}

// Bagian Edit-----------------------------------------------------------------------

function tampileditIncomingInternal(id)
{
	EditDropzone_Click();
	$('#id_update_incoming_internal').val(id);

	$('#table_file_edit').load("../../back-end/incoming_internal/view_update_file_incoming_internal_code.php",{id:id});
	$('#java_script').load('../../page-admin/incoming_internal/js_incoming_internal.php');

	$.post("../../back-end/incoming_internal/view_update_incoming_internal_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_ii = JSON.parse(data);
				$('#txt_noagenda_update').val(view_update_ii.no_agenda_ii);
				$('#txt_nosurat_update').val(view_update_ii.no_surat_ii);
				$('#txt_tglentry_update').val(view_update_ii.tgl_entry_ii);
				$('#txt_pengirim_update').val(view_update_ii.pengirim_ii);
				$('#txt_status_update').val(view_update_ii.status_ii);
				$('#txt_perihal_update').val(view_update_ii.perihal_ii);
				$('#txt_disposisi_direksi_update').val(view_update_ii.disposisi_direksi_ii);
				$('#txt_tembusan_update').val(view_update_ii.tembusan_ii);
				$('#txt_disposisi_id_update').val(view_update_ii.disposisi_id);
				$('#txt_tgl_disposisi_id_update').val(view_update_ii.tgl_disposisi_id_ii);
				$('#txt_isi_disposisi_id_update').val(view_update_ii.isi_disposisi_ii);

			});

	$('#modaledit').modal('show');
}

function editIncomingInternal()
{
	var txt_noagenda = $('#txt_noagenda_update').val();
	var txt_nosurat = $('#txt_nosurat_update').val();
	var txt_tglentry = $('#txt_tglentry_update').val();
	var txt_pengirim = $('#txt_pengirim_update').val();
	var txt_status	= $('#txt_status_update').val();
	var txt_perihal	= $('#txt_perihal_update').val();
	var txt_disposisi_direksi = $('#txt_disposisi_direksi_update').val();
	var txt_tembusan = $('#txt_tembusan_update').val();
	var txt_disposisi_id = $('#txt_disposisi_id_update').val();
	var txt_tgl_disposisi_id = $('#txt_tgl_disposisi_id_update').val();
	var txt_isi_disposisi_id = $('#txt_isi_disposisi_id_update').val();

	$.ajax({
		url:"../../back-end/incoming_internal/update_incoming_internal_code.php",
		type:'POST',
		data: { txt_noagenda : txt_noagenda,
			txt_nosurat : txt_nosurat,
			txt_tglentry : txt_tglentry,
			txt_pengirim : txt_pengirim,
			txt_status : txt_status,
			txt_perihal : txt_perihal,
			txt_disposisi_direksi : txt_disposisi_direksi,
			txt_tembusan : txt_tembusan,
			txt_disposisi_id : txt_disposisi_id,
			txt_tgl_disposisi_id : txt_tgl_disposisi_id,
			txt_isi_disposisi_id : txt_isi_disposisi_id
		},

		success:function(data,status){
			showIncomingInternal();
		}

	});
}


function resetupdate_IncomingInternal()
{
			$('#txt_nosurat').val(null);
			$('#txt_pengirim').val(null);
			$('#txt_status').val(null);
			$('#txt_perihal').val(null);
			$('#txt_disposisi_direksi').val(null);
			$('#txt_tembusan').val(null);
			$('#txt_disposisi_id').val(null);
			$('#txt_isi_disposisi_id').val(null);
			resetDate();
			$('#txt_nosurat').focus();
}

// Bagian View & Download

function viewdownloadIncomingInternal(id)
{

	$('#table_file_view').load("../../back-end/incoming_internal/view_download_file_incoming_internal_code.php",{id:id});
	$('#java_script').load('../../page-admin/incoming_internal/js_incoming_internal.php');

	$.post("../../back-end/incoming_internal/view_update_incoming_internal_code.php", {id:id}, 
		function(data,status)
			{
				var view_update_ii = JSON.parse(data);
				$('#txt_noagenda_view').val(view_update_ii.no_agenda_ii);
				$('#txt_nosurat_view').val(view_update_ii.no_surat_ii);
				$('#txt_tglentry_view').val(view_update_ii.tgl_entry_ii);
				$('#txt_pengirim_view').val(view_update_ii.pengirim_ii);
				$('#txt_status_view').val(view_update_ii.status_ii);
				$('#txt_perihal_view').val(view_update_ii.perihal_ii);
				$('#txt_disposisi_direksi_view').val(view_update_ii.disposisi_direksi_ii);
				$('#txt_tembusan_view').val(view_update_ii.tembusan_ii);
				$('#txt_disposisi_id_view').val(view_update_ii.disposisi_id);
				$('#txt_tgl_disposisi_id_view').val(view_update_ii.tgl_disposisi_id_ii);
				$('#txt_isi_disposisi_id_view').val(view_update_ii.isi_disposisi_ii);

			});

	$('#modalviewdownload').modal('show');
}

function downloadIncomingInternal(id_ag,id_file)
{

	$.post("../../back-end/incoming_internal/download_file_incoming_internal_code.php", 
		{
			id_ag:id_ag,
			id_file:id_file
		}, 
		function(data,status)
			{

			});
}

// Bagian Delete -------------------------------------------------------------
function deletefileeditIncomingInternal(id_ag,id_file,file_name)
{
    swal({
            title: "Delete Information",
            text: "Apakah Anda Yakin Menghapus File : '"+file_name+"' Dengan No Agenda : '"+id_ag+"' ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) 
            {
				$.ajax({
					url:"../../back-end/incoming_internal/delete_file_incoming_internal_code.php",
					type:"POST",
					data:{ 
						id_ag:id_ag,
						id_file:id_file
						},
					success:function(data,status){
						swal("Information", "Delete File Berhasil", "success");
						$('#table_file_edit').load("../../back-end/incoming_internal/view_update_file_incoming_internal_code.php",{id:id_ag});
						$('#java_script').load('../../page-admin/incoming_internal/js_incoming_internal.php');
					}
				});
            } 
        });
}