function removeallfile_Dropzone()
{
      $('#txt_tglentry').val(null);
      $('#txt_nopeg').val(null);
      $('#txt_kepada').val(null);
      $('#txt_prepared').val(null);
      $('#txt_subject').val(null);
      $('#txt_tambahan').val(null);

      resetDate();
      
  var count_file = myDropzone.files.length;
  if (count_file > 0)
  {
    myDropzone.removeAllFiles(true);
    myDropzone.removeAllFiles(false);
  }

}

function AddDropzone_Click(){
  var Dropzone1 = $("#add_dropzone")[0].dropzone;
  var Dropzone2 = $("#edit_dropzone")[0].dropzone;

if ( Dropzone1 || Dropzone2) 
  {
    myDropzone.destroy();
    AddDropzone_Set();
  }
else
  {
    AddDropzone_Set();
  }


}

function AddDropzone_Set()
{
  var add_dropzone_v = new Dropzone(
        '#add_dropzone',
        {           
          init: function() {
            var submitButtonadd = document.querySelector("#submit-outgoing-internal") 
                myDropzone = this; // closure

          var addII =  function(file) {
              var count_file = myDropzone.files.length;
              addOutgoingInternal();

              if(count_file > 0)
              {
                myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                myDropzone.options.autoProcessQueue = true;
                submitButtonadd.removeEventListener("click", addII);
              }
              else
              {
                resetOutgoingInternal();
                $('#modalinsert').modal('hide');
                $('#close-add').trigger('click');
                $('#modalInsertSucces').modal('show');
                submitButtonadd.removeEventListener("click", addII);
                AddDropzone_Click();
              }


            }
            submitButtonadd.addEventListener("click", addII);

            this.on("complete", function(file) {
                  myDropzone.removeFile(file);
            });

            this.on("queuecomplete", function(file) {
                  myDropzone.options.autoProcessQueue = false;
                  myDropzone.disable();
                  myDropzone.enable();
                  resetOutgoingInternal();
                  $('#modalinsert').modal('hide');
                  $('#close-add').trigger('click');
                  $('#modalInsertSucces').modal('show');
                  AddDropzone_Click();
            });

            this.on("error", function(file) {
              swal("Error", "Maaf Sebagian File Tidak Dapat Diupload (File MaxSize : 2MB & Total MaxFile : 10 File)", "error");
              myDropzone.removeFile(file);
            });

            this.on("canceled", function(file) {
              swal("Error", "Maaf Sebagian File Gagal Diupload !", "error");
              myDropzone.removeFile(file);
            });

          }
        });
}

function EditDropzone_Click(){

  var Dropzone1 = $("#add_dropzone")[0].dropzone;
  var Dropzone2 = $("#edit_dropzone")[0].dropzone;

  if ( Dropzone1 || Dropzone2) 
    {
      myDropzone.destroy();
      EditDropzone_Set();
    }
  else
    {
      EditDropzone_Set();
    }

}

function EditDropzone_Set()
{
  var edit_dropzone_v = new Dropzone(
        '#edit_dropzone',
        {           
          init: function() {
            var submitButton = document.querySelector("#edit-outgoing-internal")
                myDropzone = this; // closure
            var editII = function(file) {
              var nosurat = $('#txt_nosurat_update').val();

              swal({
                title: "Edit Information",
                text: "Apakah Anda Yakin Update Data Dengan No Surat : '"+nosurat+"' ?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) 
                {
                  var count_file = myDropzone.files.length;
                  editOutgoingInternal();

                  if(count_file > 0)
                  {
                    myDropzone.processQueue(); // Tell Dropzone to process all queued files.
                    myDropzone.options.autoProcessQueue = true;
                    submitButton.removeEventListener("click", editII);
                  }
                  else
                  {
                    resetupdate_OutgoingInternal();
                    $('#modaledit').modal('hide');
                    $('#close-edit').trigger('click');
                    $('#modalEditSucces').modal('show');
                    submitButton.removeEventListener("click", editII);
                  }
                } 
              });

            }

            submitButton.addEventListener("click", editII);

            this.on("complete", function(file) {
                  myDropzone.removeFile(file);
            });

            this.on("queuecomplete", function(file) {
                  myDropzone.options.autoProcessQueue = false;
                  myDropzone.disable();
                  myDropzone.enable();
                  resetupdate_OutgoingInternal();
                  $('#modaledit').modal('hide');
                  $('#close-edit').trigger('click');
                  $('#modalEditSucces').modal('show');
            });

            this.on("error", function(file) {
              swal("Error", "Maaf Sebagian File Tidak Dapat Diupload (File MaxSize : 2MB & Total MaxFile : 10 File)", "error");
              myDropzone.removeFile(file);
            });

            this.on("canceled", function(file) {
              swal("Error", "Maaf Sebagian File Gagal Diupload !", "error");
              myDropzone.removeFile(file);
            });

          }
        });
}
