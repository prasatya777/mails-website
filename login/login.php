<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link href="../image/garuda.png" rel="shortcut icon">

<!--===============================================================================================-->
 	<link rel="stylesheet" type="text/css" href="../font/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="../css/util.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">

  	<!-- Cek Login -->
  	<script type="text/javascript" src="../js/ajax_login.js"></script>

   	<!-- Sweet Alert -->
  	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- =============================================================================================== -->
</head>
<body style="background-color: #ffffff;">
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form id="form-login" class="login100-form validate-form" method="POST" onsubmit="return cek_Login()">
					<span class="login100-form-title p-b-43">
						Login
					</span>
					
					
					<div class="wrap-input100 validate-input" data-validate="Username is required" >
						<input class="input100" type="text" name="txtusername" id="txtusername">
						<span class="focus-input100"></span>
						<span class="label-input100">No Pegawai</span>
					</div>
					
					
					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="txtpassword" id="txtpassword">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>

					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<div class="contact100-form-checkbox">
						</div>
					</div>
			
						<button class="login100-form-btn">
							Login
						</button>			
				</form>
			

				<div class="login100-more" style="background-image: url('../image/page.jpg');">
				</div>
			</div>
		</div>
	</div>

	<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="../js/main.js"></script>

<script>
  $(document).ready(function() {

    $.post("../back-end/login/show_logout_code.php", {}, 
    function(data)
      {
        if(data == 1)
        {
          swal("Information", "Logout Berhasil", "success");
        }
      });
  });
</script>

</body>
</html>