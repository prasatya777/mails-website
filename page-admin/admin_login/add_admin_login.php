<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>
      <form method="POST" enctype="multipart/form-data" >
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_no_pegawai" id="txt_no_pegawai" class="form-control" placeholder="No Pegawai" required="required">
                  <label for="txt_no_pegawai">No Pegawai</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
                <div class="form-label-group">
                  <input type="text" name="txt_nama" id="txt_nama" class="form-control" placeholder="Nama Lengkap" required="required">
                  <label for="txt_nama">Nama Lengkap</label>
                </div>
          </div>

          <div class="form-group">
                <div class="form-label-group">
                  <input type="email" name="txt_email" id="txt_email" class="form-control" placeholder="Email Address" required="required">
                  <label for="txt_email">Email Address</label>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <select name="txt_kelamin" autocomplete="off" required="required" id="txt_kelamin" class="form-control" placeholder="Jenis Kelamin" ' required="required">
                      <option value='' disabled selected hidden>---Jenis Kelamin---</option>
                      <option value='p' >Pria</option>
                      <option value='w' >Wanita</option>        
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <select name="txt_hakakses" autocomplete="off" required="required" id="txt_hakakses" class="form-control" placeholder="Hak Akses" ' required="required">
                      <option value='' disabled selected hidden>---Hak Akses---</option>
                      <option value='admin' >Admin</option>
                      <option value='user' >User</option>        
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="password" name="txt_password" id="txt_password" class="form-control" placeholder="Password" required="required">
                  <label for="txt_password">Password</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="password" name="txt_conf_password" id="txt_conf_password" class="form-control" placeholder="Confirm Password" required="required">
                  <label for="txt_conf_password">Confirm Password</label>
                </div>
              </div>
            </div>
          </div>
      </form>
</body>

</html>
