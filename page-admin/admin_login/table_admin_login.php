<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Garuda-Incoming Internal</title>

</head>

<body>
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Surat Incoming Internal</div>
          <div class="card-body">

          <div class='addbutton'>
            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="modalInsert_Click()">
              <i class='fa fa-plus-circle fa-lg fa-fw' aria-hidden='true'></i>Tambah User
            </button>
          </div>

            <div id="record_table">
              <?php include '../../back-end/admin_login/show_admin_login_code.php'; ?>
            </div>

          </div>
          <div class="card-footer small text-muted">Garuda&copy;2018</div>
        </div>

  <div id="boxmodal_admin_login">
    <?php include '../../page-admin/admin_login/modal_admin_login/modal_admin_login.php' ?>
  </div>


</body>

</html>
