<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Garuda-Incoming Internal</title>

</head>

<body>
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Export File Surat Masuk dan Keluar</div>
          <div class="card-body">

              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-label-group">
                      <select name="txt_tbl_export_excel" autocomplete="off" required="required" id="txt_tbl_export_excel" class="form-control" placeholder="Tabel Export Yang Diinginkan" '>
                          <option value='' disabled selected hidden>---Pilih Tabel Export Yang Diinginkan---</option>
                          <option value='ii' >Surat Incoming Internal</option>
                          <option value='ie' >Surat Incoming External</option>
                          <option value='oi' >Surat Outgoing Internal</option>
                          <option value='oe' >Surat Outgoing External</option>            
                        </select>
                    </div>
                  </div>
                </div>
              </div>

                <button type="button" class="btn btn-primary" data-toggle="modal" onclick="exportClick()" >
                  <i class='fa fa-download fa-lg fa-fw' aria-hidden='true'></i>Export Excel
                </button>

          </div>
          <div class="card-footer small text-muted">Garuda&copy;2018</div>
        </div>



  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
