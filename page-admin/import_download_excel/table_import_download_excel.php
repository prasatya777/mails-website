<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Garuda-Incoming Internal</title>

</head>

<body>
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Import File Surat Masuk dan Keluar</div>
          <div class="card-body">

            <form method="POST" id="ImportExcel">

              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-label-group">
                      <select name="txt_tbl_import_excel" autocomplete="off" required="required" id="txt_tbl_import_excel" class="form-control" placeholder="Tabel Import Yang Dituju" '>
                          <option value='' disabled selected hidden>---Pilih Tabel Import Yang Dituju---</option>
                          <option value='ii' >Surat Incoming Internal</option>
                          <option value='ie' >Surat Incoming External</option>
                          <option value='oi' >Surat Outgoing Internal</option>
                          <option value='oe' >Surat Outgoing External</option>            
                        </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-label-group">
                      <input type="file" name="ImportExcelFile" autocomplete="off" id="ImportExcelFile" class="form-control" placeholder="Masukkan File Excel">
                      <label for="ImportExcelFile">Masukkan File Excel</label>
                    </div>
                  </div>
                </div>
              </div>

            </form>

                <button type="button" class="btn btn-primary" data-toggle="modal" onclick="importClick()" >
                  <i class='fa fa-upload fa-lg fa-fw' aria-hidden='true'></i>Import Excel
                </button>

          </div>
          <div class="card-footer small text-muted">Garuda&copy;2018</div>
        </div>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
