<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_noagenda_view" autocomplete="off" id="txt_noagenda_view" class="form-control" placeholder="No Agenda" readonly="readonly">
                  <label for="txt_noagenda_view">No Agenda</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nosurat_view" id="txt_nosurat_view" class="form-control" placeholder="No Surat" autofocus="autofocus" readonly="readonly">
                  <label for="txt_nosurat_view">No Surat</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tglentry_view" autocomplete="off" id="txt_tglentry_view" class="form-control" placeholder="Tanggal Entry (Bulan/Tanggal/Tahun)" readonly="readonly">
                  <label for="txt_tglentry_view">Tanggal Entry (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_pengirim_view" id="txt_pengirim_view" class="form-control" placeholder="Pengirim" readonly="readonly">
                  <label for="txt_pengirim_view">Pengirim</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_status_view" id="txt_status_view" class="form-control" placeholder="Status" readonly="readonly">
                  <label for="txt_status_view">Status</label>
                </div>
              </div>
            </div>
          </div>


          <div class="form-group">
            <div class="form-label-group">
              <input type="text" name="txt_perihal_view" required="required" id="txt_perihal_view" class="form-control" placeholder="Perihal" readonly="readonly">
              <label for="txt_perihal_view">Perihal</label>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_disposisi_direksi_view" id="txt_disposisi_direksi_view" class="form-control" placeholder="Disposisi Direksi" readonly="readonly">
                  <label for="txt_disposisi_direksi_view">Disposisi Direksi</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_tembusan_view" id="txt_tembusan_view" class="form-control" placeholder="Tembusan" readonly="readonly">
                  <label for="txt_tembusan_view">Tembusan</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_disposisi_id_view" id="txt_disposisi_id_view" class="form-control" placeholder="Disposisi ID" readonly="readonly">
                  <label for="txt_disposisi_id_view">Disposisi ID</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tgl_disposisi_id_view" id="txt_tgl_disposisi_id_view" class="form-control" placeholder="Tanggal Disposisi ID (Bulan/Tanggal/Tahun)" readonly="readonly">
                  <label for="txt_tgl_disposisi_id_view">Tanggal Disposisi ID (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <textarea rows="7" name="txt_isi_disposisi_id_view" autocomplete="off" id="txt_isi_disposisi_id_view" class="form-control" placeholder="Isi Disposisi" readonly="readonly"></textarea>
              <label for="txt_isi_disposisi_id_view">Isi Disposisi</label>
            </div>
          </div>

          <div class="form-group">
            <div id="table_file_view">
            </div>
          </div>

</body>

</html>
