<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>
  <!-- Modal Add Incoming External-->
  <div class="modal fade" id="modalinsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Tambah Surat Masuk Incoming External</h5>
          <button id="close-add" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php include 'add_incoming_external.php'; ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="removeallfile_Dropzone()">Batal</button>
          <button id='submit-incoming-external' name='submit-incoming-external' type="button" class="btn btn-primary" >Simpan</button>
        </div>
      </div>
    </div>
  </div>

<!-- Modal Insert Succes Incoming External-->
<div class="modal fade" id="modalInsertSucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Insert Information</h5>
        <button id="close-insert-succes" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Insert Data Berhasil
      </div>
      <div class="modal-footer">
        <button id='ok-incoming-external' type="button" class="btn btn-primary" data-dismiss="modal" onclick="jv_modal_succes()">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete Incoming External-->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Information</h5>
        <button id="close-delete" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div id="modal-body-delete" class="modal-body"></div>

      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button id='delete-incoming-external' name='delete-incoming-external' type="button" class="btn btn-primary" data-dismiss="modal" onclick="jv_modal_delete()">Hapus</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete Succes Incoming External-->
<div class="modal fade" id="modalDeleteSucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Information</h5>
        <button id="close-delete-succes" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div id="modal-delete-succes" class="modal-body">
        Hapus Data Berhasil
      </div>
      <div class="modal-footer">
        <button id='delete-succes-incoming-external' type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

  <!-- Modal Edit Incoming External-->
  <div class="modal fade" id="modaledit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Edit Surat Masuk Incoming External</h5>
          <button id="close-edit" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php include 'edit_incoming_external.php'; ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="removeallfile_Dropzone()" >Batal</button>
          <button id='edit-incoming-external' name='edit-incoming-external' type="button" class="btn btn-primary">Update</button>
          <input type="hidden" id="id_update_incoming_external" >
        </div>
      </div>
    </div>
  </div>

<!-- Modal Edit Succes Incoming External-->
<div class="modal fade" id="modalEditSucces" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Information</h5>
        <button id="close-edit-succes" type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Update Data Berhasil
      </div>
      <div class="modal-footer">
        <button id='edit-succes-incoming-External' type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal View & Download Incoming External-->
  <div class="modal fade" id="modalviewdownload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">View Incoming External</h5>
          <button id="close-viewdownload" type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php include 'view_incoming_external.php'; ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        </div>
      </div>
    </div>
  </div>




  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin.min.js"></script>

</body>

</html>