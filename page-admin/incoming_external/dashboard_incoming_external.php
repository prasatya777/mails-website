<?php
    include '../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
      session_start();
      ob_start();
    }

    include '../../page-admin/authentication/authenc_code.php';
    
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Garuda - Incoming External</title>
  <link href="../../image/garuda.png" rel="shortcut icon">

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="../../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">

  <!--Icon Textbox-->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

  <!-- Popup -->
  <script type="text/javascript" src="../../back-end/popup/popup.js"></script>

  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="../../css/main_css.css">

  <!-- Dropzone -->
  <link rel="stylesheet" type="text/css" href="../../vendor/dropzone/dropzone.css" />
  <script type="text/javascript" src="../../vendor/dropzone/dropzone_incoming_external.js"></script>
  <script type="text/javascript" src="../../js/dropzoneclick_incoming_external.js"></script>

  <!-- Ajax -->
  <script type="text/javascript" src="../../js/ajax_incoming_external.js"></script>

  <!-- Nav Menu -->
  <script type="text/javascript" src="../../js/js_navigation.js"></script>

  <!-- Sweet Alert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
    <img src="../../image/garuda.png" style="width:50px">
    <a class="navbar-brand mr-1" href="../../page-admin/home/dashboard_home.php" style="padding-left: 10px"> Garuda Indonesia</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div id="WelcomeUser" style="float:right; margin-left:3px;"></div>
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="#" onclick="getnopegLogin()" >Settings</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">

      <li class="nav-item active">
        <a class="nav-link" href="#" onclick="nav_home()">
          <i class="fas fa-fw fa-home"></i>
          <span>Home</span>
        </a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="btton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Incoming Letter</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="#" onclick="nav_ii()">Internal</a> 
          <a class="dropdown-item" href="#" onclick="nav_ie()">Eksternal</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-envelope-open"></i>
          <span>Outgoing Letter</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="#" onclick="nav_oi()">Internal</a>
          <a class="dropdown-item" href="#" onclick="nav_oe()">Eksternal</a>
        </div>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="#" onclick="nav_print()">
          <i class="fas fa-fw fa-print"></i>
          <span>Print Report</span>
        </a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="#" onclick="nav_exp()">
          <i class="fas fa-fw fa-download"></i>
          <span>Import & Export Excel</span>
        </a>
      </li>

    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

      <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item active">Surat Incoming External - Garuda</li>
        </ol>

        <!-- DataTables Example -->
        <div id="dashbordIncomingExternal">
          <?php include'../../page-admin/incoming_external/table_incoming_external.php'; ?>
        </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Garuda 2019</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

<?php include '../../page-admin/public_user/modal_publicuser.php'; ?>

<div id="java_script">
  <?php include '../../page-admin/incoming_external/js_incoming_external.php'; ?>
</div>

  </body>

  </html>
