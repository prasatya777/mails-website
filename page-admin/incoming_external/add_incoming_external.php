<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Dropzone -->
  <link rel="stylesheet" type="text/css" href="../../vendor/dropzone/dropzone.css" />
  <script type="text/javascript" src="../../vendor/dropzone/dropzone_incoming_external.js"></script>
  <script type="text/javascript" src="../../js/dropzoneclick_incoming_external.js"></script>

</head>

<body>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_noagenda" autocomplete="off" id="txt_noagenda" class="form-control" placeholder="No Agenda" readonly="readonly">
                  <label for="txt_noagenda">No Agenda</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nosurat" id="txt_nosurat" class="form-control" placeholder="No Surat" autofocus="autofocus">
                  <label for="txt_nosurat">No Surat</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tglentry" autocomplete="off" id="txt_tglentry" class="form-control" placeholder="Tanggal Entry (Bulan/Tanggal/Tahun)">
                  <label for="txt_tglentry">Tanggal Entry (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_pengirim" id="txt_pengirim" class="form-control" placeholder="Pengirim">
                  <label for="txt_pengirim">Pengirim</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_status" id="txt_status" class="form-control" placeholder="Status">
                  <label for="txt_status">Status</label>
                </div>
              </div>
            </div>
          </div>


          <div class="form-group">
            <div class="form-label-group">
              <input type="text" name="txt_perihal" required="required" id="txt_perihal" class="form-control" placeholder="Perihal">
              <label for="txt_perihal">Perihal</label>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_disposisi_direksi" id="txt_disposisi_direksi" class="form-control" placeholder="Disposisi Direksi">
                  <label for="txt_disposisi_direksi">Disposisi Direksi</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_tembusan" id="txt_tembusan" class="form-control" placeholder="Tembusan">
                  <label for="txt_tembusan">Tembusan</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_disposisi_id" id="txt_disposisi_id" class="form-control" placeholder="Disposisi ID">
                  <label for="txt_disposisi_id">Disposisi ID</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tgl_disposisi_id" id="txt_tgl_disposisi_id" class="form-control" placeholder="Tanggal Disposisi ID (Bulan/Tanggal/Tahun)">
                  <label for="txt_tgl_disposisi_id">Tanggal Disposisi ID (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <textarea rows="7" name="txt_isi_disposisi_id" autocomplete="off" id="txt_isi_disposisi_id" class="form-control" placeholder="Isi Disposisi"></textarea>
              <label for="txt_isi_disposisi_id">Isi Disposisi</label>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <form action="" class="dropzone" id="add_dropzone">
              </form>
            </div>
          </div>

</body>

</html>
