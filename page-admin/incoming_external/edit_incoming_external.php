<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_noagenda_update" autocomplete="off" id="txt_noagenda_update" class="form-control" placeholder="No Agenda" readonly="readonly">
                  <label for="txt_noagenda_update">No Agenda</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nosurat_update" id="txt_nosurat_update" class="form-control" placeholder="No Surat" autofocus="autofocus">
                  <label for="txt_nosurat_update">No Surat</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tglentry_update" autocomplete="off" id="txt_tglentry_update" class="form-control" placeholder="Tanggal Entry (Bulan/Tanggal/Tahun)">
                  <label for="txt_tglentry_update">Tanggal Entry (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_pengirim_update" id="txt_pengirim_update" class="form-control" placeholder="Pengirim">
                  <label for="txt_pengirim_update">Pengirim</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_status_update" id="txt_status_update" class="form-control" placeholder="Status">
                  <label for="txt_status_update">Status</label>
                </div>
              </div>
            </div>
          </div>


          <div class="form-group">
            <div class="form-label-group">
              <input type="text" name="txt_perihal_update" required="required" id="txt_perihal_update" class="form-control" placeholder="Perihal">
              <label for="txt_perihal_update">Perihal</label>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_disposisi_direksi_update" id="txt_disposisi_direksi_update" class="form-control" placeholder="Disposisi Direksi">
                  <label for="txt_disposisi_direksi_update">Disposisi Direksi</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_tembusan_update" id="txt_tembusan_update" class="form-control" placeholder="Tembusan">
                  <label for="txt_tembusan_update">Tembusan</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_disposisi_id_update" id="txt_disposisi_id_update" class="form-control" placeholder="Disposisi ID">
                  <label for="txt_disposisi_id_update">Disposisi ID</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tgl_disposisi_id_update" id="txt_tgl_disposisi_id_update" class="form-control" placeholder="Tanggal Disposisi ID (Bulan/Tanggal/Tahun)">
                  <label for="txt_tgl_disposisi_id_update">Tanggal Disposisi ID (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <textarea rows="7" name="txt_isi_disposisi_id_update" autocomplete="off" id="txt_isi_disposisi_id_update" class="form-control" placeholder="Isi Disposisi"></textarea>
              <label for="txt_isi_disposisi_id_update">Isi Disposisi</label>
            </div>
          </div>

          <div class="form-group">
            <div id="table_file_edit">
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <form action="" class="dropzone" id="edit_dropzone">
              </form>
            </div>
          </div>
        
</body>

</html>
