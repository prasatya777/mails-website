<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Dropzone -->
  <link rel="stylesheet" type="text/css" href="../../vendor/dropzone/dropzone.css" />
  <script type="text/javascript" src="../../vendor/dropzone/dropzone_outgoing_internal.js"></script>
  <script type="text/javascript" src="../../js/dropzoneclick_outgoing_internal.js"></script>

</head>

<body>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nosurat_update" id="txt_nosurat_update" class="form-control" placeholder="No Surat" autofocus="autofocus" readonly="readonly">
                  <label for="txt_nosurat_update">No Surat</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tglentry_update" autocomplete="off" id="txt_tglentry_update" class="form-control" placeholder="Tanggal Entry (Bulan/Tanggal/Tahun)">
                  <label for="txt_tglentry_update">Tanggal Entry (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nopeg_update" id="txt_nopeg_update" class="form-control" placeholder="No Pegawai">
                  <label for="txt_nopeg_update">No Pegawai</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_kepada_update" id="txt_kepada_update" class="form-control" placeholder="Kepada">
                  <label for="txt_kepada_update">Kepada</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_prepared_update" id="txt_prepared_update" class="form-control" placeholder="Status">
                  <label for="txt_prepared_update">Prepared By</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <textarea rows="4" name="txt_subject_update" autocomplete="off" id="txt_subject_update" class="form-control" placeholder="Subject"></textarea>
              <label for="txt_subject_update">Subject</label>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <textarea rows="4" name="txt_tambahan_update" autocomplete="off" id="txt_tambahan_update" class="form-control" placeholder="Tambahan"></textarea>
              <label for="txt_tambahan_update">Tambahan</label>
            </div>
          </div>

          <div class="form-group">
            <div id="table_file_edit">
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <form action="" class="dropzone" id="edit_dropzone">
              </form>
            </div>
          </div>

</body>

</html>
