<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Garuda-Outgoing External</title>

</head>

<body>
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Tabel Surat Outgoing External</div>
          <div class="card-body">

          <div class='addbutton'>
            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="modalInsert_Click()">
              <i class='fa fa-plus-circle fa-lg fa-fw' aria-hidden='true'></i>Tambah Surat Outgoing External
            </button>
          </div>

            <div id="record_table">
              <?php include '../../back-end/outgoing_external/show_outgoing_external_code.php'; ?>
            </div>

          </div>
          <div class="card-footer small text-muted">Garuda&copy;2018</div>
        </div>

  <div id="boxmodal_outgoingexternal">
    <?php include '../../page-admin/outgoing_external/modal_outgoing_external/modal_outgoing_external.php' ?>
  </div>


</body>

</html>
