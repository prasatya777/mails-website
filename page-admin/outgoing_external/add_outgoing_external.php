<?php 
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Dropzone -->
  <link rel="stylesheet" type="text/css" href="../../vendor/dropzone/dropzone.css" />
  <script type="text/javascript" src="../../vendor/dropzone/dropzone_outgoing_external.js"></script>
  <script type="text/javascript" src="../../js/dropzoneclick_outgoing_external.js"></script>

</head>

<body>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nosurat" id="txt_nosurat" class="form-control" placeholder="No Surat" autofocus="autofocus" readonly="readonly">
                  <label for="txt_nosurat">No Surat</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tglentry" autocomplete="off" id="txt_tglentry" class="form-control" placeholder="Tanggal Entry (Bulan/Tanggal/Tahun)">
                  <label for="txt_tglentry">Tanggal Entry (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nopeg" id="txt_nopeg" class="form-control" placeholder="No Pegawai">
                  <label for="txt_nopeg">No Pegawai</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_kepada" id="txt_kepada" class="form-control" placeholder="Kepada">
                  <label for="txt_kepada">Kepada</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_prepared" id="txt_prepared" class="form-control" placeholder="Status">
                  <label for="txt_prepared">Prepared By</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <textarea rows="4" name="txt_subject" autocomplete="off" id="txt_subject" class="form-control" placeholder="Subject"></textarea>
              <label for="txt_subject">Subject</label>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <form action="" class="dropzone" id="add_dropzone">
              </form>
            </div>
          </div>

</body>

</html>
