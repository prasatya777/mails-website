<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nosurat_view" id="txt_nosurat_view" class="form-control" placeholder="No Surat" autofocus="autofocus" readonly="readonly">
                  <label for="txt_nosurat_view">No Surat</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="date" name="txt_tglentry_view" autocomplete="off" id="txt_tglentry_view" class="form-control" placeholder="Tanggal Entry (Bulan/Tanggal/Tahun)" readonly="readonly">
                  <label for="txt_tglentry_view">Tanggal Entry (Bulan/Tanggal/Tahun)</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_nopeg_view" id="txt_nopeg_view" class="form-control" placeholder="No Pegawai" readonly="readonly">
                  <label for="txt_nopeg_view">No Pegawai</label>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_kepada_view" id="txt_kepada_view" class="form-control" placeholder="Kepada" readonly="readonly">
                  <label for="txt_kepada_view">Kepada</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <div class="form-label-group">
                  <input type="text" name="txt_prepared_view" id="txt_prepared_view" class="form-control" placeholder="Status" readonly="readonly">
                  <label for="txt_prepared_view">Prepared By</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-label-group">
              <textarea rows="4" name="txt_subject_view" autocomplete="off" id="txt_subject_view" class="form-control" placeholder="Subject" readonly="readonly"></textarea>
              <label for="txt_subject_view">Subject</label>
            </div>
          </div>

          <div class="form-group">
            <div id="table_file_view">
            </div>
          </div>

</body>

</html>
