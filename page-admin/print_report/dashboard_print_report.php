<?php
    include '../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
      session_start();
      ob_start();
    }

    include '../../page-admin/authentication/authenc_code.php';
    
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Garuda - Print Laporan Surat Masuk Dan Surat Keluar</title>
  <link href="../../image/garuda.png" rel="shortcut icon">

  <!-- Custom fonts for this template-->
  <link href="../../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template-->
  <link href="../../css/sb-admin.css" rel="stylesheet">

  <!--Icon Textbox-->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">

  <!-- Main -->
  <link rel="stylesheet" type="text/css" href="../../css/main_css.css">

  <!-- Select 2-->
  <link rel="stylesheet" href="../../vendor/select2/dist/css/select2.min.css"/>

  <!-- Ajax - Import Download -->
  <script type="text/javascript" src="../../js/ajax_print_report.js"></script>

  <!-- Datepicker -->
  <link rel="stylesheet" href="../../vendor/datepicker/css/datepicker.css">

  <!-- Nav Menu -->
  <script type="text/javascript" src="../../js/js_navigation.js"></script>

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
    <img src="../../image/garuda.png" style="width:50px">
    <a class="navbar-brand mr-1" href="../../page-admin/home/dashboard_home.php" style="padding-left: 10px"> Garuda Indonesia</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div id="WelcomeUser" style="float:right; margin-left:3px;"></div>
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="#" onclick="getnopegLogin()" >Settings</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">

      <li class="nav-item active">
        <a class="nav-link" href="#" onclick="nav_home()">
          <i class="fas fa-fw fa-home"></i>
          <span>Home</span>
        </a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="btton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-envelope"></i>
          <span>Incoming Letter</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="#" onclick="nav_ii()">Internal</a> 
          <a class="dropdown-item" href="#" onclick="nav_ie()">Eksternal</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-envelope-open"></i>
          <span>Outgoing Letter</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <a class="dropdown-item" href="#" onclick="nav_oi()">Internal</a>
          <a class="dropdown-item" href="#" onclick="nav_oe()">Eksternal</a>
        </div>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="#" onclick="nav_print()">
          <i class="fas fa-fw fa-print"></i>
          <span>Print Report</span>
        </a>
      </li>

      <li class="nav-item active">
        <a class="nav-link" href="#" onclick="nav_exp()">
          <i class="fas fa-fw fa-download"></i>
          <span>Import & Export Excel</span>
        </a>
      </li>

    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

      <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item active">Garuda - Print Report Surat Masuk Dan Surat Keluar</li>
        </ol>

        <!-- Import Example -->
        <div id="dashbordDelete">
          <?php include'../../page-admin/print_report/table_print_report.php'; ?>
        </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Garuda 2019</span>
            </div>
          </div>
        </footer>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

<?php include '../../page-admin/public_user/modal_publicuser.php'; ?>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin.min.js"></script>

  <!--Javascript Select Option With Searching-->
  <script src="../../vendor/select2/dist/js/select2.min.js"></script>

  <!-- Datepicker -->
  <script src="../../vendor/datepicker/js/bootstrap-datepicker.js"></script>

  <!-- Ajax Welcome - User -->
  <script src="../../js/ajax_publicuser.js"></script>

  <!-- Java Script Setting Import -->
  <script>
      $(document).ready(function () {
          $("#txt_select_report").select2({
              placeholder: "---Pilih Surat Yang Ingin Dicetak---",
              width: '100%'
          });

      $(".datepicker1").datepicker({
        format: "dd/mm/yyyy"
      }).on('changeDate', function (e) {
          $('.datepicker1').datepicker('hide');
          });

      $(".datepicker2").datepicker({
        format: "dd/mm/yyyy"
          }).on('changeDate', function (e) {
          $('.datepicker2').datepicker('hide');
          });
              
      });
  </script>

  </body>

  </html>
