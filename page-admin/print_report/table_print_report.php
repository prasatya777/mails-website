<?php
  include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- <title>Garuda-Incoming Internal</title> -->

</head>

<body>
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Print Report Surat Masuk dan Keluar</div>
          <div class="card-body">

              <div class="form-group">
                <div class="form-row">
                  <div class="col-md-4">
                    <div class="form-label-group">
                      <select name="txt_select_report" autocomplete="off" required="required" id="txt_select_report" class="form-control" placeholder="Surat Yang Ingin Dicetak" '>
                          <option value='' disabled selected hidden>---Surat Yang Ingin Dicetak---</option>
                          <option value='ii' >Surat Incoming Internal</option>
                          <option value='ie' >Surat Incoming External</option>
                          <option value='oi' >Surat Outgoing Internal</option>
                          <option value='oe' >Surat Outgoing External</option>            
                        </select>
                    </div>
                  </div>
                </div>
              </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-4">
                <div class="form-label-group">
                  <input type="text" name="txt_tgl_awal" autocomplete="off" id="txt_tgl_awal" class="form-control datepicker1" placeholder="Dari Tanggal (Bulan/Tanggal/Tahun)" readonly="readonly" style="background-color:#FFFF">
                  <label for="txt_tgl_awal">Tanggal Awal</label>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-label-group">
                  <input type="text" name="txt_to" id="txt_to" class="form-control" readonly="readonly" placeholder="To">
                  <label for="txt_to" style="text-align: center; font-weight: bold;">To</label>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-label-group">
                  <input type="text" name="txt_tgl_akhir" autocomplete="off" id="txt_tgl_akhir" class="form-control datepicker2" placeholder="Sampai Tanggal (Bulan/Tanggal/Tahun)" readonly="readonly" style="background-color:#FFFF">
                  <label for="txt_tgl_akhir">Tanggal Akhir</label>
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="form-row">
              <div class="col-md-2">
                <div class="form-label-group">
                  <input type="text" name="txt_thn_laporan" id="txt_thn_laporan" class="form-control" placeholder="Tahun Laporan" autocomplete="off">
                  <label for="txt_thn_laporan">Tahun Laporan</label>
                </div>
              </div>
            </div>
          </div>

                <button type="button" class="btn btn-primary" data-toggle="modal" onclick="printClick()" >
                  <i class='fa fa-print fa-lg fa-fw' aria-hidden='true'></i>Print
                </button>

          </div>
          <div class="card-footer small text-muted">Garuda&copy;2018</div>
        </div>


</body>

</html>
