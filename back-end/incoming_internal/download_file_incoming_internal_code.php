<?php
	include('../../koneksi/koneksi.php');
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
			$id_ii = $_POST['id_ag'];
			$id_file = $_POST['id_file'];

			$response = array();

			try {
				$pdo = $conn->prepare('SELECT 
										file_incoming_internal,
										filename_incoming_internal,
										filetype_incoming_internal
										FROM 
										tbl_file_incoming_internal
										WHERE 
										file_no_agenda_ii =:id1
										AND 
										file_id_incoming_internal =:id2
										');
				$pdo->execute(array(':id1' => $id_ii, ':id2' => $id_file));
				$row = $pdo->fetch(PDO::FETCH_OBJ);

				$file = $row->file_incoming_internal;
				$name = $row->filename_incoming_internal;
				$type = $row->filetype_incoming_internal;

				header('Cache-Control: public');
				header('Content-Description: File Garuda Incoming Internal');
				header('Content-Disposition: attachment; filename="'.$name.'"');
				header('Content-type: '.$type);
				echo $file;
				exit;

			} catch (PDOexception $e) {
			   die();
			}	
?>
