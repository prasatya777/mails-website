<?php
	include('../../koneksi/koneksi.php');
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
			$id_ii = $_POST['id_ag'];
			$id_file = $_POST['id_file'];
			try {
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('Delete from tbl_file_incoming_internal where 
										file_no_agenda_ii = :id1
										AND 
										file_id_incoming_internal = :id2
										');
				$deletedata = array(':id1' => $id_ii, ':id2' => $id_file);
				$pdo->execute($deletedata);
			} catch (PDOexception $e) {
			   die();
			}	
?>