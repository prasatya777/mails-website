<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);

			$no_srt = $_POST['txt_nosurat'];
			$tgl_en	= $_POST['txt_tglentry'];
			$peg	= $_POST['txt_nopeg'];
			$to	= $_POST['txt_kepada'];
			$pre	= $_POST['txt_prepared'];
			$sub = $_POST['txt_subject'];
			$tamb = $_POST['txt_tambahan'];

			$_SESSION['id_oi'] = $no_srt;

			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('INSERT INTO tbl_outgoing_internal (
										no_surat_oi, tgl_oi, subject_oi, kepada_oi, 
										nopeg_oi, prepared_by_oi, keterangan_oi) 
										VALUES 
										(
										:nosrt, :tglen, :sb, 
										:kpd, :nop, :pr, :ktrngn)');
				$insertdata = array(
									':nosrt' => $no_srt, ':tglen' => $tgl_en, 
									':sb' => $sub, ':kpd' => $to, 
									':nop' => $peg, ':pr' => $pre,
									':ktrngn' => $tamb
									);
				$pdo->execute($insertdata);

			} catch (PDOexception $e) {
				print "Insert data gagal: " . $e->getMessage() . "<br/>";
			   die();
			}
?>