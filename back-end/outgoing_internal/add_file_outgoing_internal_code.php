<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	

			$id_ii = $_SESSION['id_oi'];
			$name = $_FILES['file']['name'];
			$type = $_FILES['file']['type'];
			$data = file_get_contents($_FILES['file']['tmp_name']);
			
			try {

	            $result = $conn->prepare('SELECT file_id_outgoing_internal FROM tbl_file_outgoing_internal ORDER BY file_id_outgoing_internal DESC LIMIT 1');
	            $result->execute();
	            $count = $result->rowCount();

				if($count>0)
				{
		            $row=$result->fetch(PDO::FETCH_OBJ);
		            $noakhir = intval($row->file_id_outgoing_internal);
		           	$noakhir = $noakhir+1;
		            $number_auto = $noakhir;
	        	}
	        	else
	        	{
	        		$number_auto = '1';
	        	}

			} catch (PDOexception $e) {
			   die();
			}

			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('INSERT INTO tbl_file_outgoing_internal (	
										file_no_surat_oi, file_id_outgoing_internal, 
										file_outgoing_internal, 
										filename_outgoing_internal, 
										filetype_outgoing_internal) 
										VALUES 
										(:id, :ii, :file, :namefile, :typefile)'
									);
				$insertdata = array(':id' => $id_ii, ':ii' => $number_auto, 
									':file' => $data, ':namefile' => $name, 
									':typefile' => $type);
				$pdo->execute($insertdata);

			} catch (PDOexception $e) {
				print "Insert data gagal: " . $e->getMessage() . "<br/>";
			   die();
			}
?>