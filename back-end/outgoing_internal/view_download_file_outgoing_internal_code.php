<?php
	include '../../page-admin/authentication/authenc_code.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>

<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}
	$id_view_file_ii = $_POST['id'];
			try {

				$no=0;
				$data = "
			            <div class='table-responsive'>
			            <div id='showrecord'>
			              <table class='table table-striped table-bordered' id='dataTable_download' width='98%' cellspacing='0'>
			                <thead>
			                  <tr>
			                    <th width = 8%>No</th>
			                    <th width = 84%>Namafile</th>
			                    <th width = 8%>Download</th>
			                  </tr>
			                </thead>
			                <tfoot>
			                  <tr>
			                    <th width = 8%>No</th>
			                    <th width = 84%>Namafile</th>
			                    <th width = 8%>Download</th>
			                  </tr>
			                </tfoot>
			                <tbody>
				";

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('SELECT 
										file_no_surat_oi,
										file_id_outgoing_internal,
										filename_outgoing_internal
										FROM 
										tbl_file_outgoing_internal 
										WHERE file_no_surat_oi = :id');
				$pdo->bindparam(':id',  $id_view_file_ii);
				$pdo->execute();

				while($row=$pdo->fetch(PDO::FETCH_OBJ)){
						$no = $no + 1;
				    	$data .= "
				                    <tr>
				                    	<td>$no</td>
				                    	<td>$row->filename_outgoing_internal</td>
					                    <div class='table-action'>

					                    <td>
					                      <div class='downloadmain_file button_admin_download_file'>
					                                  <a onclick='
					                                  downloadOutgoingInternal( 
					                                  \"$row->file_no_surat_oi\",
					                                  \"$row->file_id_outgoing_internal\"
					                                  )'>
					                                  <i class='fa fa-download fa-lg fa-fw' aria-hidden='true'></i></a>
					                      </div>
					                    </td>

					                    </div>
					                    
				                    </tr>
				                    ";
				}

			$data .= "
			                </tbody>
			              </table>
			            </div>
			            </div>
			";

			echo "$data";
			
			} catch (PDOexception $e) {
			   die();
			}
?>

  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin.min.js"></script>

  <script >
	$(document).ready(function() {
    	$('#dataTable_download').DataTable();
    });
  </script>

</body>

</html>
