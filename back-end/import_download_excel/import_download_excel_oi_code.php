<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
	include '../../vendor/PHPExcel-1.8/Classes/PHPExcel.php';

		$inputFile = $_FILES["ImportExcelFile"]["tmp_name"];
        $inputFileType = PHPExcel_IOFactory::identify($inputFile);
        $excelReader = PHPExcel_IOFactory::createReader($inputFileType);
        $excelObj = $excelReader->load($inputFile);

		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();
		
		for ($row = 2; $row <= $lastRow; $row++) {

			$no_srt = $worksheet->getCell('B'.$row)->getValue();
			$tgl_en	= $worksheet->getCell('A'.$row)->getValue();
			if($tgl_en == "")
			{
				$tgl_en = "0/0/0000";
			}
			$peg	= $worksheet->getCell('E'.$row)->getValue();
			$to	= $worksheet->getCell('D'.$row)->getValue();
			$pre	= $worksheet->getCell('F'.$row)->getValue();
			$sub = $worksheet->getCell('C'.$row)->getValue();
			$tamb = $worksheet->getCell('G'.$row)->getValue();

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_outgoing_internal (
										no_surat_oi, 
										tgl_oi, 
										subject_oi, 
										kepada_oi, 
										nopeg_oi, 
										prepared_by_oi, 
										keterangan_oi) 
										VALUES 
										(
										:nosrt, 
										STR_TO_DATE( :tglen, "%d/%m/%Y"), 
										:sb, 
										:kpd, 
										:nop, 
										:pr, 
										:ktrngn)');
			$insertdata = array(
									':nosrt' => $no_srt, 
									':tglen' => $tgl_en, 
									':sb' => $sub, 
									':kpd' => $to, 
									':nop' => $peg, 
									':pr' => $pre,
									':ktrngn' => $tamb
									);
			$pdo->execute($insertdata);
		}
?>