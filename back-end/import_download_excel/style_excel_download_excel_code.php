<?php

	include '../../page-admin/authentication/authenc_code.php';

	$styleTipe = array(
	  'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'=>0,
	             'wrap'=>true),
	  'fill'      => array(
	                'type' => PHPExcel_Style_Fill::FILL_SOLID,
	                'color' => array('rgb' => 'f7d21c')),
	  'borders' => array(
	          'allborders' => array(
	          'style' => PHPExcel_Style_Border::BORDER_THIN
	          )),
	  'font' => array(
	        'bold' => true,
	        'size' => 10,
	        'color' => array('rgb' => '000000'),
	        'name'  => 'Arial')
	  );
	  
	$styleOri    = array(
	  'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'=>0,'wrap' => true),

	  'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => 'ffffff')),'borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),'font' => array('size' => 11,'name'  => 'Arial'));

	$border = array('borders' => array('right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));

	$bold   = array(
	  'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,'rotation'=>0,
	             'wrap'=>true),
	  'font' => array('bold' => true,'size' => 11,'name'  => 'Arial'));
?>