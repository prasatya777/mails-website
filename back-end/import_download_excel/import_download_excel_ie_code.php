<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
	include '../../vendor/PHPExcel-1.8/Classes/PHPExcel.php';

		$inputFile = $_FILES["ImportExcelFile"]["tmp_name"];
        $inputFileType = PHPExcel_IOFactory::identify($inputFile);
        $excelReader = PHPExcel_IOFactory::createReader($inputFileType);
        $excelObj = $excelReader->load($inputFile);

		$worksheet = $excelObj->getSheet(0);
		$lastRow = $worksheet->getHighestRow();
		
		for ($row = 2; $row <= $lastRow; $row++) {

			$no_ag = $worksheet->getCell('A'.$row)->getValue();
			$no_srt = $worksheet->getCell('B'.$row)->getValue();
			$tgl_en	= $worksheet->getCell('C'.$row)->getValue();
			if($tgl_en == "")
			{
				$tgl_en = "0/0/0000";
			}
			$kirim	= $worksheet->getCell('D'.$row)->getValue();
			$sts	= $worksheet->getCell('E'.$row)->getValue();
			$hal	= $worksheet->getCell('F'.$row)->getValue();
			$dsp_dir = $worksheet->getCell('G'.$row)->getValue();
			$temb = $worksheet->getCell('H'.$row)->getValue();
			$dsp_id = $worksheet->getCell('I'.$row)->getValue();
			$tgl_dsp_id = $worksheet->getCell('J'.$row)->getValue();
			if($tgl_dsp_id == "")
			{
				$tgl_dsp_id = "0/0/0000";
			}
			$isi_dsp_id = $worksheet->getCell('K'.$row)->getValue();

			$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$pdo = $conn->prepare('INSERT INTO tbl_incoming_external (
										no_agenda_ie, 
										no_surat_ie, 
										tgl_entry_ie, 
										pengirim_ie, 
										status_ie, 
										perihal_ie, 
										disposisi_direksi_ie, 
										tembusan_ie,
										disposisi_id, 
										tgl_disposisi_id_ie, 
										isi_disposisi_ie) 
										VALUES 
										(
										:noag, 
										:nosrt, 
										STR_TO_DATE( :tglen, "%d/%m/%Y"),
										:krm, 
										:stat, 
										:hl, 
										:dspdir, 
										:tmb, 
										:dspid, 
										STR_TO_DATE( :tgldspid, "%d/%m/%Y"), 
										:isidspid)');
			$insertdata = array(
									':noag' => $no_ag, ':nosrt' => $no_srt, 
									':tglen' => $tgl_en, ':krm' => $kirim, 
									':stat' => $sts, ':hl' => $hal,
									':dspdir' => $dsp_dir, ':tmb' => $temb,
									':dspid' => $dsp_id, 
									':tgldspid' => $tgl_dsp_id, 
									':isidspid' => $isi_dsp_id);
			$pdo->execute($insertdata);
		}
?>