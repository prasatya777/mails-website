<?php
  include '../../koneksi/koneksi.php';
  if (session_status() == PHP_SESSION_NONE) 
  {
      session_start();
      ob_start();
  }

  include '../../page-admin/authentication/authenc_code.php';
  
  include '../../vendor/PHPExcel-1.8/Classes/PHPExcel.php';
  include '../../back-end/import_download_excel/style_excel_download_excel_code.php';

  $tanggal_export=date('d-m-Y');
  $objPHPExcel = new PHPExcel();

  $objPHPExcel->getActiveSheet()->setShowGridlines(false);

  $objPHPExcel->getActiveSheet()->getStyle("A1:F1")->applyFromArray($styleTipe);

  $objPHPExcel->getActiveSheet()->freezePane('A2');
  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(150);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(50);
  
  $result = $conn->query('select * from tbl_outgoing_external');

  $objPHPExcel->getActiveSheet()->setCellValue('A1','Tanggal Entry');
  $objPHPExcel->getActiveSheet()->setCellValue('B1','No Surat');
  $objPHPExcel->getActiveSheet()->setCellValue('C1','Subject');
  $objPHPExcel->getActiveSheet()->setCellValue('D1','Kepada');
  $objPHPExcel->getActiveSheet()->setCellValue('E1','No Pegawai');
  $objPHPExcel->getActiveSheet()->setCellValue('F1','Prepared By');

  $i=1;

  while($row=$result->fetch(PDO::FETCH_OBJ)){
    $i++;
    $break_tgl = explode('-',$row->tgl_oe);

    $objPHPExcel->getActiveSheet()
    ->setCellValue('A'.$i, $break_tgl[2].'/'.$break_tgl[1].'/'.$break_tgl[0]);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->no_surat_oe);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $row->subject_oe);
    $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->kepada_oe);
    $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->nopeg_oe);
    $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->prepared_by_oe);
    $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleOri);
  }

  $objPHPExcel->getActiveSheet()->setTitle('OUTGOING-EXTERNAL');

  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="File-Export-Outgoing-External('.$tanggal_export.').xlsx"');
  header('Cache-Control: max-age=0');
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');
  exit;
?>