<?php
	include '../../koneksi/koneksi.php';
  if (session_status() == PHP_SESSION_NONE) 
  {
      session_start();
      ob_start();
  }

  include '../../page-admin/authentication/authenc_code.php';
  
  include '../../vendor/PHPExcel-1.8/Classes/PHPExcel.php';
  include '../../back-end/import_download_excel/style_excel_download_excel_code.php';

  $tanggal_export=date('d-m-Y');
  $objPHPExcel = new PHPExcel();

  $objPHPExcel->getActiveSheet()->setShowGridlines(false);

  $objPHPExcel->getActiveSheet()->getStyle("A1:K1")->applyFromArray($styleTipe);

  $objPHPExcel->getActiveSheet()->freezePane('A2');

  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(100);
  $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
  $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(150);
  $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
  $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
  $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(50);
  $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
  $objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(50);
  
  $result = $conn->query('select * from tbl_incoming_internal');

  $objPHPExcel->getActiveSheet()->setCellValue('A1','No Agenda');
  $objPHPExcel->getActiveSheet()->setCellValue('B1','No Surat/Tgl Surat');
  $objPHPExcel->getActiveSheet()->setCellValue('C1','Tanggal Entry');
  $objPHPExcel->getActiveSheet()->setCellValue('D1','Pengirim');
  $objPHPExcel->getActiveSheet()->setCellValue('E1','Status');
  $objPHPExcel->getActiveSheet()->setCellValue('F1','Perihal');
  $objPHPExcel->getActiveSheet()->setCellValue('G1','Disposisi Direksi');
  $objPHPExcel->getActiveSheet()->setCellValue('H1','Tembusan');
  $objPHPExcel->getActiveSheet()->setCellValue('I1','Disposisi ID');
  $objPHPExcel->getActiveSheet()->setCellValue('J1','Tanggal Disposisi ID');
  $objPHPExcel->getActiveSheet()->setCellValue('K1','Isi Disposisi');

  $i=1;

  while($row=$result->fetch(PDO::FETCH_OBJ)){
    $i++;
    $break_tgl_en = explode('-',$row->tgl_entry_ii);
    $break_tgl_ds = explode('-',$row->tgl_disposisi_id_ii);

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $row->no_agenda_ii);
    $objPHPExcel->getActiveSheet()->getStyle('A'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $row->no_surat_ii);
    $objPHPExcel->getActiveSheet()->getStyle('B'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()
    ->setCellValue('C'.$i, $break_tgl_en[2].'/'.$break_tgl_en[1].'/'.$break_tgl_en[0]);
    $objPHPExcel->getActiveSheet()->getStyle('C'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $row->pengirim_ii);
    $objPHPExcel->getActiveSheet()->getStyle('D'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $row->status_ii);
    $objPHPExcel->getActiveSheet()->getStyle('E'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $row->perihal_ii);
    $objPHPExcel->getActiveSheet()->getStyle('F'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $row->disposisi_direksi_ii);
    $objPHPExcel->getActiveSheet()->getStyle('G'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $row->tembusan_ii);
    $objPHPExcel->getActiveSheet()->getStyle('H'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $row->disposisi_id);
    $objPHPExcel->getActiveSheet()->getStyle('I'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()
    ->setCellValue('J'.$i, $break_tgl_ds[2].'/'.$break_tgl_ds[1].'/'.$break_tgl_ds[0]);
    $objPHPExcel->getActiveSheet()->getStyle('J'.$i)->applyFromArray($styleOri);

    $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, $row->isi_disposisi_ii);
    $objPHPExcel->getActiveSheet()->getStyle('K'.$i)->applyFromArray($styleOri);
  }

  $objPHPExcel->getActiveSheet()->setTitle('INCOMING-INTERNAL');

  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="File-Export-Incoming-Internal('.$tanggal_export.').xlsx"');
  header('Cache-Control: max-age=0');
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  $objWriter->save('php://output');
  exit;
?>