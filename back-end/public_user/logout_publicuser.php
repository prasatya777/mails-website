<?php
    if (session_status() == PHP_SESSION_NONE) 
    {
      session_start();
      ob_start();
    }

    include '../../page-admin/authentication/authenc_code.php';

	session_destroy();

    if (session_status() == PHP_SESSION_NONE) 
    {
      session_start();
      ob_start();
      $_SESSION['nilai-logout'] = 1;
    }

?>