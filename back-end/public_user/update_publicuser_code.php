<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);

			$no_pg = $_POST['txt_no_pegawai'];
			$nm = $_POST['txt_nama'];
			$em	= $_POST['txt_email'];
			$kl	= $_POST['txt_kelamin'];
			$ha	= $_POST['txt_hakakses'];
			$psw	= $_POST['txt_password'];

			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				$pdo = $conn->prepare('UPDATE tbl_admin_login
												set
												nama_al =:nam, 
												email_al =:mail, 
												jk_al =:kel, 
												hakakses_al =:hk, 
												password_al =:pwd
												WHERE no_pegawai_al =:pg');
				$updatedata = array(
									':nam' => $nm, 
									':mail' => $em, 
									':kel' => $kl, 
									':hk' => $ha, 
									':pwd' => $psw,
									':pg' => $no_pg
								);
				$pdo->execute($updatedata);

			$_SESSION['username'] = $no_pg;
			$_SESSION['nama'] = $nm;
			$_SESSION['hk'] = $ha;
			} catch (PDOexception $e) {
			   die();
			}
?>