<?php
	include('../../koneksi/koneksi.php');
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
			$id_ii = $_POST['id_ag'];
			$id_file = $_POST['id_file'];
			try {
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('Delete from tbl_file_outgoing_external where 
										file_no_surat_oe = :id1
										AND 
										file_id_outgoing_external = :id2
										');
				$deletedata = array(':id1' => $id_ii, ':id2' => $id_file);
				$pdo->execute($deletedata);
			} catch (PDOexception $e) {
			   die();
			}	
?>