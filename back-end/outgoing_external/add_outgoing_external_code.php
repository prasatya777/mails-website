<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);

			$no_srt = $_POST['txt_nosurat'];
			$tgl_en	= $_POST['txt_tglentry'];
			$peg	= $_POST['txt_nopeg'];
			$to	= $_POST['txt_kepada'];
			$pre	= $_POST['txt_prepared'];
			$sub = $_POST['txt_subject'];

			$_SESSION['id_oe'] = $no_srt;

			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('INSERT INTO tbl_outgoing_external (
										no_surat_oe, tgl_oe, subject_oe, kepada_oe, 
										nopeg_oe, prepared_by_oe) 
										VALUES 
										(
										:nosrt, :tglen, :sb, 
										:kpd, :nop, :pr)');
				$insertdata = array(
									':nosrt' => $no_srt, ':tglen' => $tgl_en, 
									':sb' => $sub, ':kpd' => $to, 
									':nop' => $peg, ':pr' => $pre
									);
				$pdo->execute($insertdata);

			} catch (PDOexception $e) {
				print "Insert data gagal: " . $e->getMessage() . "<br/>";
			   die();
			}
?>