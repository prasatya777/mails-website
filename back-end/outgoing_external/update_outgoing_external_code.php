<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);

			$no_srt = $_POST['txt_nosurat'];
			$tgl_en	= $_POST['txt_tglentry'];
			$peg	= $_POST['txt_nopeg'];
			$to	= $_POST['txt_kepada'];
			$pre	= $_POST['txt_prepared'];
			$sub = $_POST['txt_subject'];

			$_SESSION['id_oe'] = $no_srt;

			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

				$pdo = $conn->prepare('UPDATE tbl_outgoing_external 
												set
												tgl_oe =:tglen, 
												subject_oe =:sb, 
												kepada_oe =:kpd, 
												nopeg_oe =:nop, 
												prepared_by_oe =:pr
												WHERE no_surat_oe =:nosrt');
				$updatedata = array(
									':tglen' => $tgl_en, 
									':sb' => $sub, 
									':kpd' => $to, 
									':nop' => $peg, 
									':pr' => $pre,
									':nosrt' => $no_srt
								);
				$pdo->execute($updatedata);

			} catch (PDOexception $e) {
			   die();
			}
?>