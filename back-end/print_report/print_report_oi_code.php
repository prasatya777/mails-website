<?php
	if (session_status() == PHP_SESSION_NONE) 
	{
	    session_start();
	    ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
	include '../../vendor/fpdf/pdf_mc_table.php';
	extract($_POST);
	$awal=$_POST['tgl_awal'];
	$akhir=$_POST['tgl_akhir'];
	$thn=$_POST['thn'];

	$break_awal = explode('/',$awal);
	$break_akhir = explode('/',$akhir);

	$awal=$break_awal[2].'-'.$break_awal[1].'-'.$break_awal[0];
	$akhir=$break_akhir[2].'-'.$break_akhir[1].'-'.$break_akhir[0];

	$pdf = new PDF_MC_Table();
	$pdf->AddPage('L','A4',0);
	$pdf->SetWidths(Array(10,30,25,90,30,20,20,52));
	$pdf->SetLineHeight(5);
	$pdf->SetAligns(Array('C','C','C','C','C','C','C','C'));

        include '../../koneksi/koneksi.php';
		$result = $conn->query('SELECT * FROM tbl_outgoing_internal WHERE tgl_oi >= "'.$awal.'" AND tgl_oi <= "'.$akhir.'" ');
		$i=0;
		$pdf->SetFont('Times','B',12);
		$pdf->Row(Array(
			'No',
			'No Surat',
			'Tanggal Entry',
			'Subject',
			'Kepada',
			'No Pegawai',
			'Prepared By',
			'Keterangan'
		));
		$pdf->SetFont('Times','',12);
		$pdf->SetAligns(Array('C','L','C','L','L','L','L','L','C','L','L'));
   		while($row=$result->fetch(PDO::FETCH_OBJ)){
   			$i++;
    		$break_tgl_en = explode('-',$row->tgl_oi);
   			$pdf->Row(Array(
   				$i,
   				$row->no_surat_oi,
   				$break_tgl_en[2].'/'.$break_tgl_en[1].'/'.$break_tgl_en[0],
   				$row->subject_oi,
   				$row->kepada_oi,
   				$row->nopeg_oi,
   				$row->prepared_by_oi,
   				$row->keterangan_oi
   			));
        }
	$pdf->AliasNbPages();

	$awal_name=$break_awal[0].'-'.$break_awal[1].'-'.$break_awal[2];
	$akhir_name=$break_akhir[0].'-'.$break_akhir[1].'-'.$break_akhir[2];

	$filename = 'Report Outgoing Internal ('.$awal_name.') To ('.$akhir_name.').pdf';
    $pdf->Output('D', $filename, true);
    exit;
?>