<?php
	if (session_status() == PHP_SESSION_NONE) 
	{
	    session_start();
	    ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
	include '../../vendor/fpdf/pdf_mc_table.php';
	extract($_POST);
	$awal=$_POST['tgl_awal'];
	$akhir=$_POST['tgl_akhir'];
	$thn=$_POST['thn'];

	$break_awal = explode('/',$awal);
	$break_akhir = explode('/',$akhir);

	$awal=$break_awal[2].'-'.$break_awal[1].'-'.$break_awal[0];
	$akhir=$break_akhir[2].'-'.$break_akhir[1].'-'.$break_akhir[0];

	$pdf = new PDF_MC_Table();
	$pdf->AddPage('L','A4',0);
	$pdf->SetWidths(Array(10,23,18,25,20,20,49,18,20,20,25,30));
	$pdf->SetLineHeight(5);
	$pdf->SetAligns(Array('C','C','C','C','C','C','C','C','C','C','C','C'));

        include '../../koneksi/koneksi.php';
		$result = $conn->query('SELECT * FROM tbl_incoming_internal WHERE tgl_entry_ii >= "'.$awal.'" AND tgl_entry_ii <= "'.$akhir.'" ');
		$i=0;
		$pdf->SetFont('Times','B',12);
		$pdf->Row(Array(
			'No',
			'No Agenda',
			'No Surat',
			'Tanggal Entry',
			'Pengirim',
			'Status',
			'Perihal',
			'Disposisi Direksi',
			'Tembusan',
			'Disposisi ID',
			'Tanggal Disposisi ID',
			'Isi Disposisi'
		));
		$pdf->SetFont('Times','',12);
		$pdf->SetAligns(Array('C','L','L','C','L','L','L','L','L','L','C','L'));
   		while($row=$result->fetch(PDO::FETCH_OBJ)){
   			$i++;
    		$break_tgl_en = explode('-',$row->tgl_entry_ii);
    		$break_tgl_ds = explode('-',$row->tgl_disposisi_id_ii);
   			$pdf->Row(Array(
   				$i,
   				$row->no_agenda_ii,
   				$row->no_surat_ii,
   				$break_tgl_en[2].'/'.$break_tgl_en[1].'/'.$break_tgl_en[0],
   				$row->pengirim_ii,
   				$row->status_ii,
   				$row->perihal_ii,
   				$row->disposisi_direksi_ii,
   				$row->tembusan_ii,
   				$row->disposisi_id,
   				$break_tgl_ds[2].'/'.$break_tgl_ds[1].'/'.$break_tgl_ds[0],
   				$row->isi_disposisi_ii
   			));
        }
	$pdf->AliasNbPages();

	$awal_name=$break_awal[0].'-'.$break_awal[1].'-'.$break_awal[2];
	$akhir_name=$break_akhir[0].'-'.$break_akhir[1].'-'.$break_akhir[2];

	$filename = 'Report Incoming Internal ('.$awal_name.') To ('.$akhir_name.').pdf';
    $pdf->Output('D', $filename, true);
    exit;
?>