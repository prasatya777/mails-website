<?php
    include '../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
      session_start();
      ob_start();
    }

    include '../../page-admin/authentication/authenc_code.php';
    
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>
<?php
	  $data =  "
                              <div class='table-responsive'>
                              <div id='showrecord'>
                                <table class='table table-striped table-bordered' id='dataTable' width='100%' cellspacing='0'>
                                  <thead>
                                    <tr>
                                      <th>No</th>
                                      <th>No Pegawai</th>
                                      <th>Nama Pegawai</th>
                                      <th>Email</th>
                                      <th>Jenis Kelamin</th>
                                      <th>Hak Akses</th>
                                      <th width='4%'>Delete</th>
                                      <th width='4%'>Edit</th>
                                    </tr>
                                  </thead>
                                  <tfoot>
                                    <tr>
                                      <th>No</th>
                                      <th>No Pegawai</th>
                                      <th>Nama Pegawai</th>
                                      <th>Email</th>
                                      <th>Jenis Kelamin</th>
                                      <th>Hak Akses</th>
                                      <th width='4%'>Delete</th>
                                      <th width='4%'>Edit</th>
                                    </tr>
                                  </tfoot>
                                  <tbody id='tbodyid'>
          ";
    $result = $conn->query('select * from tbl_admin_login');
    $i=0;
    while($row=$result->fetch(PDO::FETCH_OBJ)){
    $i++;
    if($row->jk_al == "p")
    {
      $jk="Pria";
    }
    else
    {
      $jk="Wanita";
    }
    $data .=  "
                            <tr>
                              <td>$i</td>
                              <td>$row->no_pegawai_al</td>
                              <td>$row->nama_al</td>
                              <td>$row->email_al</td>
                              <td>$jk</td>
                              <td>$row->hakakses_al</td>

                                <td>
                                  <div class='deletemain button_admin_delete'>
                                      <a onclick='popupdeleteAdminLogin(\"$row->no_pegawai_al\" ,\"$row->nama_al\")'>
                                      <i class='fa fa-trash fa-lg fa-fw' aria-hidden='true'></i></a>
                                  </div>
                                </td>

                                <td>
                                  <div class='editmain button_admin_edit'>
                                    <a onclick='tampileditAdminLogin(\"$row->no_pegawai_al\")'>
                                    <i class='fa fa-edit fa-lg fa-fw' aria-hidden='true'></i></a>
                                  </div>
                                </td>

                            </tr>
          ";
                    }
    $data .= "
                </tbody>
              </table>
            </div>
            </div>
          ";
    echo "$data";
?>
  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="../../js/demo/datatables-demo.js"></script>


</body>

</html>
