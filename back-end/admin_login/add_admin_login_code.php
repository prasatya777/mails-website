<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);

			$no_pg = $_POST['txt_no_pegawai'];
			$nm = $_POST['txt_nama'];
			$em	= $_POST['txt_email'];
			$kl	= $_POST['txt_kelamin'];
			$ha	= $_POST['txt_hakakses'];
			$psw	= $_POST['txt_password'];

			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('INSERT INTO tbl_admin_login (
										no_pegawai_al, 
										nama_al, 
										email_al, 
										jk_al, 
										hakakses_al, 
										password_al
										) 
										VALUES 
										(
										:pg, :nam, :mail, :kel, 
										:hk, :pwd
										)');
				$insertdata = array(
									':pg' => $no_pg, ':nam' => $nm, 
									':mail' => $em, ':kel' => $kl, 
									':hk' => $ha, ':pwd' => $psw
									);
				$pdo->execute($insertdata);

			} catch (PDOexception $e) {
			   die();
			}
?>