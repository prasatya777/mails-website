<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	$id_view_update = $_POST['id'];

	$response = array();
	
			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('SELECT no_pegawai_al, nama_al, email_al, jk_al, hakakses_al FROM tbl_admin_login WHERE no_pegawai_al = :id');
				$pdo->bindparam(':id', $id_view_update);
				$pdo->execute();
				$row = $pdo->fetch(PDO::FETCH_OBJ);

				$response = $row;

				echo json_encode($response);
				
			} catch (PDOexception $e) {
			   die();
			}
?>