<?php
    include '../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
      session_start();
      ob_start();
    }

	$user = $_POST['txtusername'];
	$pwd = $_POST['txtpassword'];

	$pdo = $conn->prepare('SELECT hakakses_al, nama_al FROM tbl_admin_login where no_pegawai_al = :user and password_al = :pwd');

	$pdo->execute(array(':user' => $user, ':pwd' => $pwd));

	$count = $pdo->rowcount();
	$row= $pdo->fetch(PDO::FETCH_OBJ);
	
	if($count == 0){
		echo "Salah";
	}
	else if($count == 1)
	{
		$_SESSION['username'] = $user;
		$_SESSION['nama'] = $row->nama_al;
		$_SESSION['hk'] = $row->hakakses_al;
		$_SESSION['masuk'] = 1;
		echo "Benar";
	}

?>