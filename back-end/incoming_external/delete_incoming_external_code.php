<?php
	include('../../koneksi/koneksi.php');
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
			$id = $_POST['id'];
			try {
				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('Delete from tbl_incoming_external where no_agenda_ie = :id');
				$deletedata = array(':id' => $id);
				$pdo->execute($deletedata);
			} catch (PDOexception $e) {
			   die();
			}	
?>