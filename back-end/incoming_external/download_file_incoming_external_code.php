<?php
	include('../../koneksi/koneksi.php');
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';
	
			$id_ii = $_POST['id_ag'];
			$id_file = $_POST['id_file'];

			$response = array();

			try {
				$pdo = $conn->prepare('SELECT 
										file_incoming_external,
										filename_incoming_external,
										filetype_incoming_external
										FROM 
										tbl_file_incoming_external
										WHERE 
										file_no_agenda_ie =:id1
										AND 
										file_id_incoming_external =:id2
										');
				$pdo->execute(array(':id1' => $id_ii, ':id2' => $id_file));
				$row = $pdo->fetch(PDO::FETCH_OBJ);

				$file = $row->file_incoming_external;
				$name = $row->filename_incoming_external;
				$type = $row->filetype_incoming_external;

				header('Cache-Control: public');
				header('Content-Description: File Garuda Incoming External');
				header('Content-Disposition: attachment; filename="'.$name.'"');
				header('Content-type: '.$type);
				echo $file;
				exit;

			} catch (PDOexception $e) {
			   die();
			}	
?>
