<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	extract($_POST);

			$no_ag = $_POST['txt_noagenda'];
			$no_srt = $_POST['txt_nosurat'];
			$tgl_en	= $_POST['txt_tglentry'];
			$kirim	= $_POST['txt_pengirim'];
			$sts	= $_POST['txt_status'];
			$hal	= $_POST['txt_perihal'];
			$dsp_dir = $_POST['txt_disposisi_direksi'];
			$temb = $_POST['txt_tembusan'];
			$dsp_id = $_POST['txt_disposisi_id'];
			$tgl_dsp_id = $_POST['txt_tgl_disposisi_id'];
			$isi_dsp_id = $_POST['txt_isi_disposisi_id'];

			$_SESSION['id_ii'] = $no_ag;

			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('INSERT INTO tbl_incoming_external (
										no_agenda_ie, no_surat_ie, tgl_entry_ie, pengirim_ie, 
										status_ie, perihal_ie, disposisi_direksi_ie, tembusan_ie,
										disposisi_id, tgl_disposisi_id_ie, isi_disposisi_ie) 
										VALUES 
										(
										:noag, :nosrt, :tglen, :krm, 
										:stat, :hl, :dspdir, :tmb, 
										:dspid, :tgldspid, :isidspid)');
				$insertdata = array(
									':noag' => $no_ag, ':nosrt' => $no_srt, 
									':tglen' => $tgl_en, ':krm' => $kirim, 
									':stat' => $sts, ':hl' => $hal,
									':dspdir' => $dsp_dir, ':tmb' => $temb,
									':dspid' => $dsp_id, ':tgldspid' => $tgl_dsp_id, 
									':isidspid' => $isi_dsp_id);
				$pdo->execute($insertdata);

			} catch (PDOexception $e) {
				print "Insert data gagal: " . $e->getMessage() . "<br/>";
			   die();
			}
?>