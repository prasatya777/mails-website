<?php
	include '../../koneksi/koneksi.php';
	if (session_status() == PHP_SESSION_NONE) 
	{
    	session_start();
		ob_start();
	}

	include '../../page-admin/authentication/authenc_code.php';

	$id_view_update = $_POST['id'];

	$_SESSION['id_ii'] = $id_view_update;

	$response = array();
	
			try {

				$conn ->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$pdo = $conn->prepare('SELECT * FROM tbl_incoming_external WHERE no_agenda_ie = :id');
				$pdo->bindparam(':id', $id_view_update);
				$pdo->execute();
				$row = $pdo->fetch(PDO::FETCH_OBJ);

				$response = $row;

				echo json_encode($response);
				
			} catch (PDOexception $e) {
			   die();
			}
?>