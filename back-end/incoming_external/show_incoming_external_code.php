<?php
    include '../../koneksi/koneksi.php';
    if (session_status() == PHP_SESSION_NONE) 
    {
      session_start();
      ob_start();
    }

    include '../../page-admin/authentication/authenc_code.php';
    
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

</head>

<body>
<?php
	  $data =  "
                              <div class='table-responsive'>
                              <div id='showrecord'>
                                <table class='table table-striped table-bordered' id='dataTable' width='185%' cellspacing='0'>
                                  <thead>
                                    <tr>
                                      <th>No Agenda</th>
                                      <th>No Surat/Tgl Surat</th>
                                      <th>Tanggal Entry</th>
                                      <th>Pengirim</th>
                                      <th>Status</th>
                                      <th width='15%'>Perihal</th>
                                      <th>Disposisi Direksi</th>
                                      <th>Tembusan</th>
                                      <th>Disposisi ID</th>
                                      <th>Tanggal Disposisi ID</th>
                                      <th width='20%'>Isi Disposisi</th>
                                      <th width='4%'>Delete</th>
                                      <th width='4%'>Edit</th>
                                      <th width='4%'>View</th>
                                    </tr>
                                  </thead>
                                  <tfoot>
                                    <tr>
                                      <th>No Agenda</th>
                                      <th>No Surat/Tgl Surat</th>
                                      <th>Tanggal Entry</th>
                                      <th>Pengirim</th>
                                      <th>Status</th>
                                      <th width='15%'>Perihal</th>
                                      <th>Disposisi Direksi</th>
                                      <th>Tembusan</th>
                                      <th>Disposisi ID</th>
                                      <th>Tanggal Disposisi ID</th>
                                      <th width='20%'>Isi Disposisi</th>
                                      <th width='4%'>Delete</th>
                                      <th width='4%'>Edit</th>
                                      <th width='4%'>View</th>
                                    </tr>
                                  </tfoot>
                                  <tbody id='tbodyid'>
          ";
    $result = $conn->query('select * from tbl_incoming_external');
    while($row=$result->fetch(PDO::FETCH_OBJ)){
    $break_tgl_en = explode('-',$row->tgl_entry_ie);
    $tgl_en = $break_tgl_en[2].'/'.$break_tgl_en[1].'/'.$break_tgl_en[0];

    $break_tgl_ds = explode('-',$row->tgl_disposisi_id_ie);
    $tgl_ds = $break_tgl_ds[2].'/'.$break_tgl_ds[1].'/'.$break_tgl_ds[0];
    
    $data .=  "
                            <tr>
                              <td>$row->no_agenda_ie</td>
                              <td>$row->no_surat_ie</td>
                              <td>$tgl_en</td>
                              <td>$row->pengirim_ie</td>
                              <td>$row->status_ie</td>
                              <td>$row->perihal_ie</td>
                              <td>$row->disposisi_direksi_ie</td>
                              <td>$row->tembusan_ie</td>
                              <td>$row->disposisi_id</td>
                              <td>$tgl_ds</td>
                              <td>$row->isi_disposisi_ie</td>

                                <td>
                                  <div class='deletemain button_admin_delete'>
                                      <a onclick='popupdeleteIncomingExternal(\"$row->no_agenda_ie\" ,\"$row->no_surat_ie\")'>
                                      <i class='fa fa-trash fa-lg fa-fw' aria-hidden='true'></i></a>
                                  </div>
                                </td>

                                <td>
                                  <div class='editmain button_admin_edit'>
                                    <a onclick='tampileditIncomingExternal(\"$row->no_agenda_ie\")'>
                                    <i class='fa fa-edit fa-lg fa-fw' aria-hidden='true'></i></a>
                                  </div>
                                </td>
                                
                                <td>
                                  <div class='viewmain button_admin_view'>
                                    <a onclick='viewdownloadIncomingExternal(\"$row->no_agenda_ie\")'>
                                    <i class='fa fa-eye fa-lg fa-fw' aria-hidden='true'></i></a>
                                  </div>
                                </td>

                            </tr>
          ";
                    }
    $data .= "
                </tbody>
              </table>
            </div>
            </div>
          ";
    echo "$data";
?>
  <!-- Bootstrap core JavaScript-->
  <script src="../../vendor/jquery/jquery.min.js"></script>
  <script src="../../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="../../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="../../vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="../../js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="../../js/demo/datatables-demo.js"></script>


</body>

</html>
