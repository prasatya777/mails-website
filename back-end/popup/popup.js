function deletedivisi(no,txtlabel)
{
if(confirm("Apakah Anda Yakin Menghapus Divisi : '"+txtlabel+"' Dengan ID : '"+no+"' ?"))
	{
		window.location.href='../../back-end/divisi/delete_divisi_code.php?id='+no+'';
		return true;
	}
else return false;
}

function deleteuser(txtemail,txtnamauser)
{
if(confirm("Apakah Anda Yakin Menghapus User : '"+txtemail+"' Dengan Nama User : '"+txtnamauser+"' ?"))
	{
		window.location.href='../../back-end/user/delete_user_code.php?id='+txtemail+'';
		return true;
	}
else return false;
}

function updatedivisi(no,txtlabel)
{
if(confirm("Apakah Anda Yakin Update Divisi : '"+txtlabel+"' No Divisi : '"+no+"' ?"))
	{
		return true;
	}
	else{ 
		return false;
	}
}

function updateuser(txtemail,txtnamauser)
{

if(confirmpassword() == true)
	{
	if(confirm("Apakah Anda Yakin Update User : '"+txtemail+"' Dengan Nama User : '"+txtnamauser+"' ?"))
		{
			return true;
		}
		else{ 
			return false;
		}
	}
	else
	{
		return false;
	}
}

function tampiljenisdivisi()
{
	var jnsdivisi_js = $("#txt_namadivisi option:selected").attr("jnsdivisi");
	$("#txt_jnsdivisi").val(jnsdivisi_js);
}

function confirmpassword()
{
	var password = document.getElementById("txt_passworduser").value;
	var confr_password = document.getElementById("txt_confirmpassworduser").value;
	
	if(password != confr_password)
	{
		alert('Maaf Password Tidak Sesuai !');
		$("#txt_passworduser").val("");
		$("#txt_confirmpassworduser").val("");
		document.getElementById("txt_passworduser").focus();
		return false;
	}
	else
	{
		return true;
	}
}

function cektelepon(tlpn) {
	var charCode = (tlpn.which) ? tlpn.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	return false;
}